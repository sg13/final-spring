package kr.co.mlec.p2p.dao;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.p2p.vo.SoldP2PVO;

@Repository
public class P2PDAOImpl implements P2PDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public void insert(Map<String, Object> map) {
		session.insert("p2p.dao.P2PDAO.insert", map);
	}

	@Override
	public List<P2PVO> select() {
		List<P2PVO> p2pList = session.selectList("p2p.dao.P2PDAO.selectAll");
		return p2pList;
	}

	@Override
	public P2PVO selectByp2pNo(int p2pNo) {
		P2PVO p2pVO = session.selectOne("p2p.dao.P2PDAO.selectOne", p2pNo);
		return p2pVO;
	}

	@Override
	public void insertPayedP2P(SoldP2PVO soldP2PVO) {
		session.insert("p2p.dao.P2PDAO.insertPayedP2P", soldP2PVO);
	}

	@Override
	public void insertOne(P2PVO p2pVO) {
		session.insert("p2p.dao.P2PDAO.insertOne", p2pVO);
	}

	@Override
	public void delete(int p2pNo) {
		session.delete("p2p.dao.P2PDAO.delete", p2pNo);
	}
	
//	TOP3 결과값 LIST
	@Override
	public List<Object> chart1_1() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.investCntChart"));
		return session.selectList("p2p.dao.P2PDAO.investCntChart");
	}
	
//	TOP3 결과값 LIST
	@Override
	public List<Object> chart1_2() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.investAmountChart"));
		return session.selectList("p2p.dao.P2PDAO.investAmountChart");
	}
	
	@Override
	public Map<String, Object> chart1_3_1() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.basicChart1"));
		return session.selectOne("p2p.dao.P2PDAO.basicChart1");
	}
	@Override
	public Map<String, Object> chart1_3_2() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.basicChart2"));
		return session.selectOne("p2p.dao.P2PDAO.basicChart2");
	}
	@Override
	public Map<String, Object> chart1_3_3() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.basicChart3"));
		return session.selectOne("p2p.dao.P2PDAO.basicChart3");
	}
	
	@Override
	public List<Object> chart2() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.genderChart"));
		return session.selectList("p2p.dao.P2PDAO.genderChart");
	}
	@Override
	public List<Object> chart3() {
		System.out.println(session.selectList("p2p.dao.P2PDAO.generationChart"));
		return session.selectList("p2p.dao.P2PDAO.generationChart");
	}

	@Override
	public List<SoldP2PVO> selectMyP2P(String custId) {
		List<SoldP2PVO> myP2PList = session.selectList("p2p.dao.P2PDAO.myP2P", custId);
		return myP2PList;
	}

}
