package kr.co.mlec.p2p.dao;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.p2p.vo.SoldP2PVO;

public interface P2PDAO {
	
	public void insert(Map<String, Object> map);

	public List<P2PVO> select();

	public P2PVO selectByp2pNo(int p2pNo);

	public void insertPayedP2P(@Valid SoldP2PVO soldP2PVO);

	public void insertOne(P2PVO p2pVO);

	public void delete(int p2pNo);
	
	public List<Object> chart1_1();

	public List<Object> chart1_2();

	public Map<String, Object> chart1_3_1();

	public Map<String, Object> chart1_3_2();

	public Map<String, Object> chart1_3_3();

	public List<Object> chart2();

	public List<Object> chart3();

	public List<SoldP2PVO> selectMyP2P(String custId);
}
