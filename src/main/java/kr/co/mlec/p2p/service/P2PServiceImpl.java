package kr.co.mlec.p2p.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.p2p.dao.P2PDAO;
import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.p2p.vo.SoldP2PVO;

@Service
public class P2PServiceImpl implements P2PService {

	@Autowired
	P2PDAO p2pDAO;
	
	List<P2PVO> p2pList = new ArrayList<P2PVO>();
	List<SoldP2PVO> myP2PList = new ArrayList<SoldP2PVO>();
	
	String p2p;//상품 번호 + 상품 이름
	int p2pNo;//상품 번호
	String p2pName;//상품 이름
	double roi;//연수익률
	int period;//상환기간
	int totalGoal;//투자 목표치

	@Override
	public List<P2PVO> crawling() {
		
		    // Jsoup를 이용해서 크롤링
		    try {
		    	String url = "http://www.daily-funding.com/investment/invest_list.php"; // 크롤링할 url지정
		    	Document doc = null; // Document에는 페이지의 전체 소스가 저장된다   
		       doc = Jsoup.connect(url).get();
			    // select를 이용하여 원하는 태그를 선택한다. select는 원하는 값을 가져오기 위한 중요한 기능이다.
			    Elements element = doc.select("div.invest_list");
			    // Iterator을 사용하여 하나씩 값 가져오기
			    Iterator<Element> ie1 = element.select("p.invest_title").iterator();
			    //수익률, 투자기간, 총 투자 금액
			    Iterator<Element> ie2 = element.select("em").iterator();
			   
			    while (ie1.hasNext()) {
			    	
			    	P2PVO p2pVO = new P2PVO();
			    	
			    	p2p = ie1.next().text();
			    	int idx = p2p.indexOf("]");
			    	p2pNo = Integer.parseInt(p2p.substring(1,idx).replaceAll("[^0-9]", ""));//[과 ], '호'를 제외한 번호 추출
			    	p2pName = p2p.substring(idx+1);
			    	
			    	roi = Double.parseDouble(ie2.next().text());//연수익률
			    	totalGoal = Integer.parseInt(ie2.next().text().replaceAll("[^0-9]", ""));//투자 금액	
			    	period = Integer.parseInt(ie2.next().text().replaceAll("[^0-9]", ""));//상환기간
			    	
			    	p2pVO.setP2pNo(p2pNo);
			    	p2pVO.setP2pName(p2pName);
			    	p2pVO.setRoi(roi);
			    	p2pVO.setTotalGoal(totalGoal);
			    	p2pVO.setPeriod(period);
			    	
			    	p2pList.add(p2pVO);
			    }
			    
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
			return p2pList;		    
	}
	
	@Override
	public void insertP2P(Map<String, Object> map) {
		p2pDAO.insert(map);
	}

//	selectAll
	@Override
	public List<P2PVO> selectP2P() {

		List<P2PVO> p2pList = p2pDAO.select();
		
		return p2pList;
	}

//	selectOne
	@Override
	public P2PVO P2Pdetail(int p2pNo) {
		P2PVO p2pVO = p2pDAO.selectByp2pNo(p2pNo);
		return p2pVO;
	}

	@Override
	public void insertPayedP2P(SoldP2PVO soldP2PVO) {
		p2pDAO.insertPayedP2P(soldP2PVO);
	}

	@Override
	public void insertOne(P2PVO p2pVO) {
		p2pDAO.insertOne(p2pVO);
	}

	@Override
	public void delete(int p2pNo) {
		p2pDAO.delete(p2pNo);
	}

	public List<Object> chart1_1(){
		return p2pDAO.chart1_1();
	}
	
	public List<Object> chart1_2(){
		return p2pDAO.chart1_2();
	}
	
	public Map<String,Object> chart1_3_1(){
		return p2pDAO.chart1_3_1();
	}
	
	public Map<String,Object> chart1_3_2(){
		return p2pDAO.chart1_3_2();
	}
	
	public Map<String,Object> chart1_3_3(){
		return p2pDAO.chart1_3_3();
	}
	
	public List<Object> chart2(){
		return p2pDAO.chart2();
	}
	
	public List<Object> chart3(){
		return p2pDAO.chart3();
	}

	@Override
	public List<SoldP2PVO> selectMyP2P(String custId) {
		List<SoldP2PVO> myP2PList = p2pDAO.selectMyP2P(custId);
		
		return myP2PList;
	}
	
	
}
