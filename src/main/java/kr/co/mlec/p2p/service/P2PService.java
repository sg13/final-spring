package kr.co.mlec.p2p.service;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.p2p.vo.SoldP2PVO;


public interface P2PService {

//	전체 행 조회
	public List<P2PVO> selectP2P();
	
//	삽입
	public List<P2PVO> crawling();
	public void insertP2P(Map<String, Object> map);

	public P2PVO P2Pdetail(int p2pNo);

//	결제 시 처리 transaction
	public void insertPayedP2P(SoldP2PVO soldP2PVO);

	public void insertOne(P2PVO p2pVO);

	public void delete(int p2pNo);
	
	public List<Object> chart1_1();
	public List<Object> chart1_2();
	public Map<String, Object> chart1_3_1();
	public Map<String, Object> chart1_3_2();
	public Map<String, Object> chart1_3_3();
	public List<Object> chart2();
	public List<Object> chart3();

	public List<SoldP2PVO> selectMyP2P(String custId);
}
