package kr.co.mlec.p2p.service;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class crawling {
	public static void main(String args[]) {
		
		String p2p = null;	//상품 번호 + 상품 이름
		int p2pNo = 0;//상품 번호
		String p2pName = null;//상품 이름
		double roi = 0;//연수익률
		int period = 0;//상환기간
		int totalGoal = 0;//투자 목표치
		
	    // Jsoup를 이용해서 크롤링
	    try {
	    	String url = "http://www.daily-funding.com/investment/invest_list.php"; // 크롤링할 url지정
	    	Document doc = null; // Document에는 페이지의 전체 소스가 저장된다   
	       doc = Jsoup.connect(url).get();
	    // select를 이용하여 원하는 태그를 선택한다. select는 원하는 값을 가져오기 위한 중요한 기능이다.
	    Elements element = doc.select("div.invest_list");
	    System.out.println("============================================================");
	    // Iterator을 사용하여 하나씩 값 가져오기
	    Iterator<Element> ie1 = element.select("p.invest_title").iterator();
	    //수익률
	    Iterator<Element> ie2 = element.select("em").iterator();
//	    //상환기간
//	    Iterator<Element> ie3 = element.select("em").iterator();
//	    //최대 투자 금액
//	    Iterator<Element> ie4 = element.select("em").iterator();
	   
	    while (ie1.hasNext()) {
	    	
	    	p2p = ie1.next().text();
	    	int idx = p2p.indexOf("]");
	    	p2pNo = Integer.parseInt(p2p.substring(1,idx).replaceAll("[^0-9]", ""));//[과 ], '호'를 제외한 번호 추출
	    	p2pName = p2p.substring(idx+1);
	    	
	    	roi = Double.parseDouble(ie2.next().text());//연수익률
	    	totalGoal = Integer.parseInt(ie2.next().text().replaceAll("[^0-9]", ""));//투자 금액	
	    	period = Integer.parseInt(ie2.next().text().replaceAll("[^0-9]", ""));//상환기간
	    	
	    	System.out.println(p2pNo + "\t" + p2pName + "\t"+ roi + "\t"+ totalGoal + "\t" + period);
	    	
	    }
	    	System.out.println("============================================================");
	    } catch (IOException e) {
	        e.printStackTrace();
	    }		    
	}	    
}