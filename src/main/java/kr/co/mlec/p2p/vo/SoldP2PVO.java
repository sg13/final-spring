package kr.co.mlec.p2p.vo;

import org.springframework.stereotype.Component;

@Component
public class SoldP2PVO {

	private int p2pNo;//상품 번호
	private String period;//수익기간
	private String custId;//결제한 고객 번호
	private int payAccount;//지불 계좌
	private int repayAccount;//상환 계좌
	private int investAmount;//투자금액
	private String sold_date;
	private String due_date;
	
	
	public SoldP2PVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SoldP2PVO(int p2pNo, String period, String custId, int payAccount, int repayAccount, int investAmount) {
		super();
		this.p2pNo = p2pNo;
		this.period = period;
		this.custId = custId;
		this.payAccount = payAccount;
		this.repayAccount = repayAccount;
		this.investAmount = investAmount;
	}

	public int getP2pNo() {
		return p2pNo;
	}

	public void setP2pNo(int p2pNo) {
		this.p2pNo = p2pNo;
	}
	
	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public int getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(int payAccount) {
		this.payAccount = payAccount;
	}

	public int getRepayAccount() {
		return repayAccount;
	}

	public void setRepayAccount(int repayAccount) {
		this.repayAccount = repayAccount;
	}
	
	public int getInvestAmount() {
		return investAmount;
	}

	public void setInvestAmount(int investAmount) {
		this.investAmount = investAmount;
	}
	

	@Override
	public String toString() {
		return "soldP2PVO [p2pNo=" + p2pNo + ", period=" + period + ", custId=" + custId + ", payAccount=" + payAccount
				+ ", repayAccount=" + repayAccount + ", investAmount=" + investAmount + "]";
	}

	public String getDue_date() {
		return due_date;
	}

	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}

	public String getSold_date() {
		return sold_date;
	}

	public void setSold_date(String sold_date) {
		this.sold_date = sold_date;
	}
}
