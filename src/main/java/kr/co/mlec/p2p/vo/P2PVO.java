package kr.co.mlec.p2p.vo;

import org.springframework.stereotype.Component;

@Component
public class P2PVO {
	
	private int p2pNo;//상품 번호
	private String p2pName;//상품 이름
	private double roi;//연수익률
	private int period;//상환기간
	private int totalGoal;// 총 투자 금액
	private int currentAmount;//현 투자 금액
	private int ratio;//비율
	
	public P2PVO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public P2PVO(int p2pNo, String p2pName, double roi, int period, int totalGoal, int currentAmount, int ratio) {
		super();
		this.p2pNo = p2pNo;
		this.p2pName = p2pName;
		this.roi = roi;
		this.period = period;
		this.totalGoal = totalGoal;
		this.currentAmount = currentAmount;
		this.ratio = ratio;
	}

	@Override
	public String toString() {
		return "P2PVO [p2pNo=" + p2pNo + ", p2pName=" + p2pName + ", roi=" + roi + ", period=" + period + ", totalGoal="
				+ totalGoal + ", currentAmount=" + currentAmount + ", ratio =" + ratio +"]";
	}

	public int getP2pNo() {
		return p2pNo;
	}

	public void setP2pNo(int p2pNo) {
		this.p2pNo = p2pNo;
	}

	public String getP2pName() {
		return p2pName;
	}

	public void setP2pName(String p2pName) {
		this.p2pName = p2pName;
	}

	public double getRoi() {
		return roi;
	}

	public void setRoi(double roi) {
		this.roi = roi;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getTotalGoal() {
		return totalGoal;
	}

	public void setTotalGoal(int totalGoal) {
		this.totalGoal = totalGoal;
	}

	public int getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(int currentAmount) {
		this.currentAmount = currentAmount;
	}


	public int getRatio() {
		return ratio;
	}


	public void setRatio(int ratio) {
		this.ratio = ratio;
	}
	
	
}
