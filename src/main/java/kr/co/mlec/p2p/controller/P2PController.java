package kr.co.mlec.p2p.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.mlec.account.service.AccountService;
import kr.co.mlec.account.vo.AccountVO;
import kr.co.mlec.calculator.service.CalcService;
import kr.co.mlec.calculator.vo.CalcVO;
import kr.co.mlec.member.service.MemberService;
import kr.co.mlec.member.vo.MemberVO;
import kr.co.mlec.p2p.service.P2PService;
import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.p2p.vo.SoldP2PVO;

@Controller
public class P2PController {

	@Autowired
	private P2PService p2pService;
	
	@Autowired
	private CalcService calcService;
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private MemberService memberService;
	
//	크롤링하여 INSERT하는 용
//	@GetMapping("/p2p")
	public void crawling() {
		
		List<P2PVO> p2pList = p2pService.crawling();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("p2pList", p2pList);
		
		p2pService.insertP2P(map);
	}
	
	@GetMapping("/p2p")
	public ModelAndView list(Model model) {
		
		List<P2PVO> p2pList = p2pService.selectP2P();
		ModelAndView mav = new ModelAndView("p2p/list");
		
		mav.addObject("p2pList", p2pList);
		model.addAttribute("p2pVO", new P2PVO());
		
		return mav;
	}
	
//	아예 조회시 차트 값까지도 나오게	
	@ResponseBody
	@GetMapping("/p2pChart")
	public Map<String, Object> chart(){
		Map<String,Object> chartMap = new HashMap();
		chartMap.put("chart1_1", p2pService.chart1_1());
		chartMap.put("chart1_2", p2pService.chart1_2());
		chartMap.put("chart1_3_1", p2pService.chart1_3_1());
		chartMap.put("chart1_3_2", p2pService.chart1_3_2());
		chartMap.put("chart1_3_3", p2pService.chart1_3_3());
		chartMap.put("chart2", p2pService.chart2());
		chartMap.put("chart3", p2pService.chart3());
		
		return chartMap;
	}
	
	@ResponseBody
	@PostMapping("/p2p")
	public Map<String, Object> calculator( @RequestParam("roi") double roi, 
										   @RequestParam("period") int period,
										   @RequestParam("value") int value ) {
		
		List<CalcVO> calcList = calcService.calculator(roi, period, value);
		Map<String, Object> map = new HashMap<String, Object>();
		
//		key값을 calcList로 calcList 자체를 map에 저장
		map.put("calcList", calcList);
		
		return map;
		
	}
	
    @GetMapping("/invest/order")
	public ModelAndView investForm(@RequestParam("p2pNo") int p2pNo,
							 	   @RequestParam("value") int value,
							 	   @RequestParam("period") int period,
							 	   HttpServletRequest request) {
//	   1. sesson에 있는 id값 받아오기
		HttpSession session = request.getSession();
		MemberVO userVO = (MemberVO) session.getAttribute("userVO");
		String id = userVO.getId();
		
//		1-1. id를 이용하여 간편 비밀번호 정보 불러오기
		String simpPwd = memberService.selectPwd(id);
		
		ModelAndView mav = new ModelAndView("invest/order");
//		2. 세션 id로 계좌 값 불러오기(지불 계좌, 상환계좌): 계좌 selectBox 출력용
		List<AccountVO> accountList = accountService.select(id);
		mav.addObject("accountList", accountList);
		
//		3. p2pNo 공유영역에 올리기
		mav.addObject("simpPwd", simpPwd);
		mav.addObject("p2pNo", p2pNo);
		mav.addObject("period", period);
		
//	 	4. boardVO를 공유영역에 올림
		mav.addObject("invAmount", value);
		
		return mav;
	}
   
   @PostMapping("/invest/order")
	public String insertPayedP2P(@RequestParam("p2pNo") int p2pNo,
								 @RequestParam("period") String period,
						 	     @RequestParam("invAmount") int investAmount,
						 	     @RequestParam("payAccountBank") String payAccountBank,
						 	     @RequestParam("repayAccount") String repayAccount,
						 	     HttpServletRequest request,
						 	     SoldP2PVO soldP2PVO,
						 	     AccountVO accountVO) {
//	   1. 상품 결제 테이블 처리
		HttpSession session = request.getSession();
		MemberVO userVO = (MemberVO) session.getAttribute("userVO");
		String custId = userVO.getId();
		
		String name = memberService.selectName(custId);
		
		//지불 계좌
		int payAccount = Integer.parseInt(payAccountBank.split(",")[0]);
		//지불 계좌 은행
		String payBank = payAccountBank.split(",")[1];
	   
	    soldP2PVO.setCustId(custId);
	    soldP2PVO.setPeriod(period);
	    soldP2PVO.setInvestAmount(investAmount);
	    soldP2PVO.setP2pNo(p2pNo);
	    soldP2PVO.setPayAccount(payAccount);
	    int repayAccount2 = Integer.parseInt(repayAccount);
	    soldP2PVO.setRepayAccount(repayAccount2);
	    
		p2pService.insertPayedP2P(soldP2PVO);
//		2. temp_account 테이블 balance 처리
//		   temp_account에 변동이 생기면 자동으로 account 테이블도 값 처리됨
//		   bank, account, cust_Id를 통한 name, 투자금액

		accountVO.setBank(payBank);
		accountVO.setAccount(payAccount);
		accountVO.setName(name);
		accountVO.setInvestAmount(investAmount);
		
		System.out.println(investAmount);
		
		accountService.updateBalance(accountVO);
		return "redirect:/invest/orderComplete/" + p2pNo + "/" + investAmount;
				
	}
   
    @GetMapping("/invest/orderComplete/{p2pNo}/{invAmount}")
	public String orderComplete(@PathVariable("p2pNo") int p2pNo,
								@PathVariable("invAmount") int invAmount,
								Model model) {
		
    	System.out.println("test5");
//    	p2pNo로 해당 상품 정보 다 조회 가능
		P2PVO p2pVO = p2pService.P2Pdetail(p2pNo);

		model.addAttribute("invAmount", invAmount);
		model.addAttribute("p2pVO", p2pVO);
		
		return "invest/orderComplete";
	}
    
    @PostMapping("/p2pNew")
	public String insertP2P(P2PVO p2pVO) {

		p2pService.insertOne(p2pVO);
		
		return "redirect:/p2p";
	}
    
    @ResponseBody
    @DeleteMapping("/p2p/{p2pNo}")
    public Map<String, String> delete(@PathVariable("p2pNo") int p2pNo){
    	
    	p2pService.delete(p2pNo);
    	
    	Map<String, String> map = new HashMap();
    	map.put("msg", p2pNo + "번 상품이 삭제되었습니다.");
    	
    	return map;
    }
    
    @GetMapping("/mypage")
	public ModelAndView list(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		MemberVO userVO = (MemberVO) session.getAttribute("userVO");
		String custId = userVO.getId();
		
		List<SoldP2PVO> myP2PList = p2pService.selectMyP2P(custId);
		
		ModelAndView mav = new ModelAndView("member/mypage");
		mav.addObject("myP2PList", myP2PList);
		
		return mav;
	}
    
    @ResponseBody
	@PostMapping("/custP2P")
	public Map<String, Object> calculator( @RequestParam("id") String custId) {
		
		List<SoldP2PVO> custP2PList = p2pService.selectMyP2P(custId);
		Map<String, Object> map = new HashMap<String, Object>();
		
//		key값을 calcList로 calcList 자체를 map에 저장
		map.put("custP2PList", custP2PList);
		
		return map;
		
	}
    
    
}
