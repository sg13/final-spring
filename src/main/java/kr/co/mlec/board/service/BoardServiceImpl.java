package kr.co.mlec.board.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.board.dao.BoardDAO;
import kr.co.mlec.board.vo.BoardVO;

@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardDAO boardDAO;
	
	public List<BoardVO> selectBoard() {
		List<BoardVO> boardList = boardDAO.select(); 
		return boardList;
	}

	public void insertBoard(BoardVO board) {
		boardDAO.insert(board);
	}

	public BoardVO detailBoard(int boardNo) {
		return boardDAO.selectByNo(boardNo);
	}

	public void deleteBoard(int boardNo) {
		boardDAO.deleteByNo(boardNo);
	}

	public void updateBoard(BoardVO board) {
		boardDAO.updateByNo(board);
	}

}
