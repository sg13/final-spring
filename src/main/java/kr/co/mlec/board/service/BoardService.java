package kr.co.mlec.board.service;

import java.util.List;

import kr.co.mlec.board.vo.BoardVO;

public interface BoardService {
	
	List<BoardVO> selectBoard();
	void insertBoard(BoardVO board);
	
//	no가지고 한 행 조회 또는 삭제해야하기에
	BoardVO detailBoard(int boardNo);
	void deleteBoard(int boardNo);
	void updateBoard(BoardVO board);
	
}
