package kr.co.mlec.board.dao;

import java.util.List;

import kr.co.mlec.board.vo.BoardVO;

public interface BoardDAO {
	
	public List<BoardVO> select();
	public void insert(BoardVO board);
	public BoardVO selectByNo(int boardNo);
	public void deleteByNo(int boardNo);
	public void updateByNo(BoardVO board);
}
