package kr.co.mlec.board.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.board.vo.BoardVO;

@Repository
public class BoardDAOImpl implements BoardDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	public List<BoardVO> select() {
		List<BoardVO> list = session.selectList("board.dao.BoardDAO.selectAll");
		return list;
	}

	public void insert(BoardVO board) {
		session.insert("board.dao.BoardDAO.insert", board);
	}

	public BoardVO selectByNo(int boardNo) {
		BoardVO board = session.selectOne("board.dao.BoardDAO.selectByNo", boardNo);
		return board;
	}

	public void deleteByNo(int boardNo) {
		session.delete("board.dao.BoardDAO.deleteByNo", boardNo);
	}
	
	public void updateByNo(BoardVO board){
		session.update("board.dao.BoardDAO.update", board);
	}

}
