package kr.co.mlec.board.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.mlec.board.service.BoardService;
import kr.co.mlec.board.vo.BoardVO;

@Controller
public class BoardController {
	
	@Autowired
	private BoardService service;
	
	@RequestMapping("/board")
	public ModelAndView list() {
		
		List<BoardVO> boardList = service.selectBoard();
		
		ModelAndView mav = new ModelAndView("board/list");
		mav.addObject("boardList", boardList);
		
		return mav;
	}
	
	@RequestMapping(value="/board/write", method=RequestMethod.GET)
	public String writeForm(Model model) {

		model.addAttribute("boardVO", new BoardVO());
		
		return "board/write";
	}
	
	@PostMapping("/board/write")
	public String write(@Valid BoardVO board, BindingResult result) {
		
		if(result.hasErrors()) {
			
			return "board/write";
		}
		
		service.insertBoard(board);
		
		return "redirect:/board";
	}
	
//	map이 map이 아니라 json으로 던지는게 responseBody다
	@ResponseBody
	@DeleteMapping("/board/{no}")
	public Map<String, String> delete(@PathVariable("no") int boardNo) {
		service.deleteBoard(boardNo);
		
		Map<String, String> map =  new HashMap<String, String>();
		
//		map에 데이터 넣는 것
		map.put("msg", boardNo + "번 게시물이 삭제되었습니다");
//		@responseBody가 있기 때문에 map을 return 하긴 하지만 map형태가 아니라 json 형태이다
		return map;
	}
	
	@GetMapping("/board/{no}")
	public ModelAndView detail(@PathVariable("no")int boardNo) {
		
		BoardVO board = service.detailBoard(boardNo);
		
		ModelAndView mav = new ModelAndView("board/detail");
		mav.addObject("board", board);
		
		return mav;
	}
	
	
//	@PathVariable uri로 넘어오는 변수값을 int no에 저장
//	jsp로 보여줄 필요 없기에 데이터만 공유영역에 올릴 Model model 객체 필요
	@GetMapping("/board/modify/{no}")
	public String modifyForm(@PathVariable("no") int no, Model model) {
		
//		detailBoard로 no에 해당하는 정보 DB에서 불러와서 boardVO에 저장
		BoardVO boardVO = service.detailBoard(no);
//	 	boardVO를 공유영역에 올림
		model.addAttribute("boardVO", boardVO);
		return "board/modify";
	}
	
	@PostMapping("/board/modify/{no}")
	public String modify(@PathVariable("no") int no, @Valid BoardVO board, BindingResult result) {
		
		board.setNo(no);
		
		if(result.hasErrors()) {
			return "board/modify";
		}
		service.updateBoard(board);
		
		return "redirect:/board";
	}
}
