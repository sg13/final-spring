package kr.co.mlec.member.dao;

import javax.validation.Valid;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.member.vo.MemberVO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	public MemberVO login(MemberVO member) {
		return session.selectOne("member.dao.MemberDAO.login", member);
	}

	@Override
	public String selectName(String custId) {
		return session.selectOne("member.dao.MemberDAO.selectName", custId);
	}

	@Override
	public int IdExsistence	(MemberVO memberVO) {
		return session.selectOne("member.dao.MemberDAO.IdExsistence", memberVO);
	}

	@Override
	public void delete(MemberVO memberVO) {
		session.delete("member.dao.MemberDAO.delete", memberVO);
		
	}

	@Override
	public void insert(@Valid MemberVO memberVO) {
		session.insert("member.dao.MemberDAO.insert", memberVO);
	}

	@Override
	public String selectPwd(String id) {
		return session.selectOne("member.dao.MemberDAO.selectPwd", id);
	}

	@Override
	public int duplicateIdCheck(String id) {
		System.out.println("dao");
		return session.selectOne("member.dao.MemberDAO.duplicateIdCheck", id);
	}
}
