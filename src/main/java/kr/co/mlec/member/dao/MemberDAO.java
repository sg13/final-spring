package kr.co.mlec.member.dao;

import javax.validation.Valid;

import kr.co.mlec.member.vo.MemberVO;

public interface MemberDAO {

	public MemberVO login(MemberVO member);

	public String selectName(String custId);

	public int IdExsistence(MemberVO memberVO);

	public void delete(MemberVO memberVO);

	public void insert(@Valid MemberVO memberVO);

	public String selectPwd(String id);

	public int duplicateIdCheck(String id);
}
