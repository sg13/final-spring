package kr.co.mlec.member.vo;


import org.springframework.stereotype.Component;

@Component
public class MemberVO {
	
	private String id;
	
	private String password;
	
	private String type;		// type == 'S'  관리자   type == 'U' 일반사용자
	
	private String simp_pwd;
	
	private String name;
	private String email;
	private int phone;
	
	private String gender;
	
	private int age;
	
	public MemberVO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MemberVO(String id, String password, String type, String simp_pwd, String name, String email, int phone,
			String gender, int age) {
		super();
		this.id = id;
		this.password = password;
		this.type = type;
		this.simp_pwd = simp_pwd;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.age = age;
	}


	@Override
	public String toString() {
		return "MemberVO [id=" + id + ", password=" + password + ", type=" + type + ", simp_pwd=" + simp_pwd + ", name="
				+ name + ", email=" + email + ", phone=" + phone + ", gender=" + gender + ", age=" + age + "]";
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

	public String getSimp_pwd() {
		return simp_pwd;
	}

	public void setSimp_pwd(String simp_pwd) {
		this.simp_pwd = simp_pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	
	
}
