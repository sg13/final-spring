package kr.co.mlec.member.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import kr.co.mlec.account.vo.AccountVO;
import kr.co.mlec.board.vo.BoardVO;
import kr.co.mlec.member.service.MemberService;
import kr.co.mlec.member.vo.MemberVO;

//sessionAttributes얘가 있어야 addAttribute로 해도 request가 아닌 session에 올라감
@SessionAttributes("userVO")
@Controller
public class LoginController {
	
	@Autowired
	private MemberService service;

//	얘는 uri
//	GET방식이니까 넘겨지는 것
//	이 uri로 들어왔고 또, 이게 get방식이면 다음 메소드를 실행해라
	@GetMapping("/login")
	public String loginForm() {
		
//		얘는 jsp
		return "member/login";
	}
	
//	얘는 똑같이 login이라는 uri인데 post 방식!
	@PostMapping("/login")
	public String login(MemberVO member, Model model) {
		
//		check하는 애(select하나 해서어쩌구 있으면 userVO 객체가 받아와지고)
//		불러왔어
		MemberVO userVO = service.login(member);
		
		if(userVO == null) {
			// 로그인 실패
			return "member/login";
		}
		
		// 로그인 성공
		
		// 세션에 등록
//		request공유영역에 올리는게 아니라 userVO로 session에 올린다!
//		원래는 addAttribute하면 request영역에 올라감
//		login은 session에 올라가야 하기에
//		여기에서 세션으로 올라간 것!
		model.addAttribute("userVO", userVO);		
		
//		메인페이지로만 가면 되니까 /ROOT uri
		return "redirect:/";
	}
	
//	세션 정보를 가져오는 것이 SessonStatus!
//	Controller2에선 이 방식을 안씀
	@GetMapping("/logout")
	public String logout(SessionStatus status) {
	
//		session에 올라온 객체 정보를 없앰!
		status.setComplete();
		
		return "redirect:/";
	}
	
	@GetMapping("/member/pwdCheck")
	public String pwdCheck(Model model) {

		model.addAttribute("memberVO", new MemberVO());
		
		return "member/pwdCheck";
	}
	
	@ResponseBody
	@DeleteMapping("/member/pwdCheck/{custId}/{pwd}")
	public Map<String, String> joinOutComplete(@PathVariable ("custId") String custId,
											   @PathVariable("pwd") String pwd) {
		
			MemberVO memberVO = new MemberVO();
		
			memberVO.setId(custId);
			memberVO.setPassword(pwd);
			
			System.out.println(memberVO);
			
			service.delete(memberVO);
			System.out.println("delete 성공");
			Map<String, String> map =  new HashMap<String, String>();
//			map.put("msg", custId + "님, 이용해주셔서 감사합니다.");
			map.put("custId", custId);
			return map;
	}
	
	@GetMapping("/member/joinoutComplete/{custId}")
	public String joinOutComplete(Model model,
								  @PathVariable("custId") String custId,
								  SessionStatus status) {
		
		status.setComplete();
		model.addAttribute("custId", custId);
		
		return "member/joinoutComplete";
		
	}
	
	@GetMapping("/member/join")
	public String joinForm(Model model) {
		
		model.addAttribute("memberVO", new MemberVO());
		System.out.println("memberVO 생성");
		
		return "member/join";
	}
	
	@PostMapping("/member/join")
	public String join(@Valid MemberVO memberVO) {
		
		service.insertJoin(memberVO);
		
		return "redirect:/member/joininComplete/" + memberVO.getId();
	}
	
	@GetMapping("/member/joininComplete/{id}")
	public ModelAndView joininComplete(@PathVariable String id) {
		
		ModelAndView mav = new ModelAndView("member/joininComplete");
		mav.addObject("id", id);
		
		return mav;
	}
	
	
	 @ResponseBody
	 @GetMapping("/duplicateIdCheck/{id}")
	 public Map<String, Object> dulicateIdCheck(@PathVariable("id") String id) {
		System.out.println("null값인가요?");
		 System.out.println(id);
		 
		int result = service.duplicateIdCheck(id);
		
		System.out.println(result);
		
		Map<String, Object> map = new HashMap<String, Object>();

		if(result == 0) {
			map.put("result", "사용 가능한 ID입니다.");
		} else {
			map.put("result", "사용 불가능한 ID입니다.");
		}
		return map;
	 }
	
}