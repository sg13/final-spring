package kr.co.mlec.member.service;

import javax.validation.Valid;

import kr.co.mlec.account.vo.AccountVO;
import kr.co.mlec.member.vo.MemberVO;

public interface MemberService {

	public MemberVO login(MemberVO member);

	public String selectName(String custId);

	public void delete(MemberVO memberVO);

	public int IdExsistence(MemberVO memberVO);

	public void insertJoin(@Valid MemberVO memberVO);

	public String selectPwd(String id);

	public int duplicateIdCheck(String id);
}
