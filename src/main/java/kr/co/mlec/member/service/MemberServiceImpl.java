package kr.co.mlec.member.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.member.dao.MemberDAO;
import kr.co.mlec.member.vo.MemberVO;

@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	private MemberDAO memberDAO;
	
//	불러오는 것
	public MemberVO login(MemberVO member) {

		return memberDAO.login(member);
	}

	@Override
	public String selectName(String custId) {
		return memberDAO.selectName(custId);
	}

	@Override
	public int IdExsistence(MemberVO memberVO) {
		return memberDAO.IdExsistence(memberVO);
	}

	@Override
	public void delete(MemberVO memberVO) {
		memberDAO.delete(memberVO);
	}

	@Override
	public void insertJoin(@Valid MemberVO memberVO) {
		memberDAO.insert(memberVO);
	}

	@Override
	public String selectPwd(String id) {
		return memberDAO.selectPwd(id);
	}

	@Override
	public int duplicateIdCheck(String id) {
		System.out.println("service");
		System.out.println(memberDAO.duplicateIdCheck(id));
		return memberDAO.duplicateIdCheck(id);
	}

}
