package kr.co.mlec.calculator.dao;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import kr.co.mlec.calculator.vo.CalcVO;

public class CalcDAOImpl implements CalcDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public CalcVO selectByNo(int p2pNo) {
		CalcVO calc = session.selectOne("calculator.dao.CalcDAO.selectByNo", p2pNo);
		return calc;
	}

}
