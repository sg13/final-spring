package kr.co.mlec.calculator.dao;
import kr.co.mlec.calculator.vo.CalcVO;

public interface CalcDAO {
	
//	DB에서 수익률, 상환기간 값을 CalcVO 주머니에 담아놔야하니까
	public CalcVO selectByNo(int p2pNo);

}
