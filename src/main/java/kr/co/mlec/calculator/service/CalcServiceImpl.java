package kr.co.mlec.calculator.service;

//https://forl.tistory.com/73
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import kr.co.mlec.calculator.vo.CalcVO;
 
@Service
public class CalcServiceImpl implements CalcService{
 
    @Override
    public List<CalcVO> calculator(double roi, int period2, int value) {
        
        //전달받은 파라미터를 숫자로 형변환
        int amount = value; // 대출원금
        double rateDouble = roi; // 대출이율
        int period = period2; // 대출기간
        rateDouble /= 100; // 금리를 %로 바꿔주는 작업
        rateDouble /= 12; // 금리를 월금리로 바꾸는 작업
        int repayMonth;	//상환원금
        
//        int totalRepayMonth = 0;//상환원금 합
//        int totalInterestRepay = 0;//세전이자 합
//        int totalTax = 0;//세금 합
//        int totalAfterTax = 0;//세후 합계 합
    
        repayMonth = Math.round((int)(amount * rateDouble * 
		                    Math.pow((double)(1+ rateDouble ), (double)period)/(Math.pow((double)(1+rateDouble),(double)period) - 1)));
        
	    ArrayList<CalcVO> list = new ArrayList<CalcVO>();
	    int remainAmount = amount; // 잔액
	    
	    for(int i =0; i< period; i++){
	    	CalcVO calc = new CalcVO();
	        
	        calc.setOriginRepay(repayMonth - (int)( remainAmount * rateDouble ));	//상환원금
	
	        calc.setIndex(i+1);
	        calc.setInterestRepay((int) ( remainAmount * rateDouble ));//이자
	        
	        calc.setTax((int) (( remainAmount * rateDouble ) * 0.275));//세금
	        calc.setAfterTax((int)(repayMonth-remainAmount*rateDouble + remainAmount*rateDouble*0.275));//세후합계
	        
	        remainAmount -= calc.getOriginRepay();
	        calc.setRemainAmount(remainAmount);
	        
//	        //각종 합 계산
//	    	totalRepayMonth += repayMonth - (int)( remainAmount * rateDouble);
//	    	totalInterestRepay += (int) ( remainAmount * rateDouble );
//	    	totalTax += (int) (( remainAmount * rateDouble ) * 0.275);
//	    	totalAfterTax += (int)(repayMonth-remainAmount*rateDouble + remainAmount*rateDouble*0.275);
//	        
//	    	int [] total = {totalRepayMonth,totalInterestRepay,totalTax,totalAfterTax};
//	    	calc.setTotal(total);
	    	
	        
	        list.add(calc); // Arraylist에 계산한 loan을 붙인다
	    }
	    return list;
    }

}
	 


