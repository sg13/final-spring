package kr.co.mlec.calculator.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.mlec.calculator.vo.CalcVO;

public interface CalcService {
	
	List<CalcVO> calculator(double roi, int period, int value);
	
}
