package kr.co.mlec.calculator.vo;

public class CalcVO{ //구조체 정의
	
	private int index;//회차
	private int originRepay;//상환원금
	private int interestRepay;//세전 이자
	private int remainAmount;//대출잔액
	private int afterTax;//세후합계
	private int tax;
	
	public CalcVO(int index, int originRepay, int interestRepay, int remainAmount, int afterTax, int tax) {
		super();
		this.index = index;
		this.originRepay = originRepay;
		this.interestRepay = interestRepay;
		this.remainAmount = remainAmount;
		this.afterTax = afterTax;
		this.tax = tax;
	}

	public CalcVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getOriginRepay() {
		return originRepay;
	}

	public void setOriginRepay(int originRepay) {
		this.originRepay = originRepay;
	}

	public int getInterestRepay() {
		return interestRepay;
	}

	public void setInterestRepay(int interestRepay) {
		this.interestRepay = interestRepay;
	}

	public int getRemainAmount() {
		return remainAmount;
	}

	public void setRemainAmount(int remainAmount) {
		this.remainAmount = remainAmount;
	}

	public int getAfterTax() {
		return afterTax;
	}

	public void setAfterTax(int afterTax) {
		this.afterTax = afterTax;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	@Override
	public String toString() {
		return "Loan [index=" + index + ", originRepay=" + originRepay + ", interestRepay=" + interestRepay
				+ ", remainAmount=" + remainAmount + ", afterTax=" + afterTax + ", tax=" + tax + "]";
	}
	
}