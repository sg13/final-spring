package kr.co.mlec.notice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.notice.dao.NoticeDAO;
import kr.co.mlec.notice.vo.NoticeVO;

@Service
public class NoticeServiceImpl implements NoticeService {

		@Autowired
		private NoticeDAO noticeDAO;
		
		public List<NoticeVO> selectBoard() {
			List<NoticeVO> noticeList = noticeDAO.select(); 
			return noticeList;
		}

		public void insertBoard(NoticeVO notice) {
			noticeDAO.insert(notice);
		}

		public NoticeVO detailBoard(int noticeNo) {
			return noticeDAO.selectByNo(noticeNo);
		}

		public void deleteBoard(int noticeNo) {
			noticeDAO.deleteByNo(noticeNo);
		}

		public void updateBoard(NoticeVO notice) {
			noticeDAO.updateByNo(notice);
		}

}
