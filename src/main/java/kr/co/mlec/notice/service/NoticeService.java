package kr.co.mlec.notice.service;


import java.util.List;

import kr.co.mlec.notice.vo.NoticeVO;

public interface NoticeService {
	
	List<NoticeVO> selectBoard();
	void insertBoard(NoticeVO notice);
	
//	no가지고 한 행 조회 또는 삭제해야하기에
	NoticeVO detailBoard(int noticeNo);
	void deleteBoard(int noticeNo);
	void updateBoard(NoticeVO notice);
	
}
