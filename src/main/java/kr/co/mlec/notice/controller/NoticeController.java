package kr.co.mlec.notice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.mlec.notice.vo.NoticeVO;
import kr.co.mlec.notice.service.NoticeService;

@Controller
public class NoticeController {
	
	@Autowired
	private NoticeService service;
	
	@RequestMapping("/notice")
	public ModelAndView list() {
		
		List<NoticeVO> noticeList = service.selectBoard();
		System.out.println(noticeList);
		ModelAndView mav = new ModelAndView("notice/list");
		mav.addObject("noticeList", noticeList);
		
		return mav;
	}
	
	@RequestMapping(value="/notice/write", method=RequestMethod.GET)
	public String writeForm(Model model) {

		model.addAttribute("noticeVO", new NoticeVO());
		
		return "notice/write";
	}
	
	@PostMapping("/notice/write")
	public String write(@Valid NoticeVO notice, BindingResult result) {
		
		if(result.hasErrors()) {
			System.out.println("여기");
			return "notice/write";
		}
		
		service.insertBoard(notice);
		
		return "redirect:/notice";
	}
	
//	map이 map이 아니라 json으로 던지는게 responseBody다
	@ResponseBody
	@DeleteMapping("/notice/{no}")
	public Map<String, String> delete(@PathVariable("no") int noticeNo) {
		service.deleteBoard(noticeNo);
		
		Map<String, String> map =  new HashMap<String, String>();
		
//		map에 데이터 넣는 것
		map.put("msg", noticeNo + "번 게시물이 삭제되었습니다");
//		@responseBody가 있기 때문에 map을 return 하긴 하지만 map형태가 아니라 json 형태이다
		return map;
	}
	
	@GetMapping("/notice/{no}")
	public ModelAndView detail(@PathVariable("no")int noticeNo) {
		
		NoticeVO notice = service.detailBoard(noticeNo);
		
		ModelAndView mav = new ModelAndView("notice/detail");
		mav.addObject("notice", notice);
		
		return mav;
	}
	
	
//	@PathVariable uri로 넘어오는 변수값을 int no에 저장
//	jsp로 보여줄 필요 없기에 데이터만 공유영역에 올릴 Model model 객체 필요
	@GetMapping("/notice/modify/{no}")
	public String modifyForm(@PathVariable("no") int no, Model model) {
		
//		detailBoard로 no에 해당하는 정보 DB에서 불러와서 noticeVO에 저장
		NoticeVO noticeVO = service.detailBoard(no);
//	 	noticeVO를 공유영역에 올림
		model.addAttribute("noticeVO", noticeVO);
		return "notice/modify";
	}
	
	@PostMapping("/notice/modify/{no}")
	public String modify(@PathVariable("no") int no, @Valid NoticeVO notice, BindingResult result) {
		
		notice.setNo(no);
		
		if(result.hasErrors()) {
			return "notice/modify";
		}
		service.updateBoard(notice);
		
		return "redirect:/notice";
	}
}
