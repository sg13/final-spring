package kr.co.mlec.notice.dao;

import java.util.List;

import kr.co.mlec.notice.vo.NoticeVO;

public interface NoticeDAO {
	
	public List<NoticeVO> select();
	public void insert(NoticeVO notice);
	public NoticeVO selectByNo(int noticeNo);
	public void deleteByNo(int noticeNo);
	public void updateByNo(NoticeVO notice);
}

