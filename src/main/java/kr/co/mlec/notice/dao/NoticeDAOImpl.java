package kr.co.mlec.notice.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.notice.vo.NoticeVO;

@Repository
public class NoticeDAOImpl implements NoticeDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	public List<NoticeVO> select() {
		List<NoticeVO> list = session.selectList("notice.dao.NoticeDAO.selectAll");
		return list;
	}

	public void insert(NoticeVO notice) {
		session.insert("notice.dao.NoticeDAO.insert", notice);
	}

	public NoticeVO selectByNo(int noticeNo) {
		NoticeVO notice = session.selectOne("notice.dao.NoticeDAO.selectByNo", noticeNo);
		return notice;
	}

	public void deleteByNo(int noticeNo) {
		session.delete("notice.dao.NoticeDAO.deleteByNo", noticeNo);
	}
	
	public void updateByNo(NoticeVO notice){
		session.update("notice.dao.NoticeDAO.update", notice);
	}

}
