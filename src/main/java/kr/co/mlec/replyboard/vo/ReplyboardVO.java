package kr.co.mlec.replyboard.vo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

public class ReplyboardVO {

	private int no;
	@Length(min=2, max=50, message="2글자이상 50글자 이하로만 입력하세요.")
	@NotEmpty(message="필수항목입니다.")
	private String title;
	private String writer;
	@NotEmpty(message="필수항목입니다.")
	private String content;
	private int viewCnt;
	private String regDate;
	private int rootid;
	private int relevel;
	
	public ReplyboardVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyboardVO(int no,
			@Length(min = 2, max = 50, message = "2글자이상 50글자 이하로만 입력하세요.") @NotEmpty(message = "필수항목입니다.") String title,
			@Pattern(regexp = "^[A-Za-z0-9ㄱ-ㅎ가-힣]*$", message = "특수기호로 시작할 수 없습니다.") @NotEmpty(message = "필수항목입니다.") String writer,
			@NotEmpty(message = "필수항목입니다.") String content, int viewCnt, String regDate, int rootid, int relevel) {
		super();
		this.no = no;
		this.title = title;
		this.writer = writer;
		this.content = content;
		this.viewCnt = viewCnt;
		this.regDate = regDate;
		this.rootid = rootid;
		this.relevel = relevel;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getViewCnt() {
		return viewCnt;
	}

	public void setViewCnt(int viewCnt) {
		this.viewCnt = viewCnt;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	
	public int getRootid() {
		return rootid;
	}

	public void setRootid(int rootid) {
		this.rootid = rootid;
	}

	public int getRelevel() {
		return relevel;
	}

	public void setRelevel(int relevel) {
		this.relevel = relevel;
	}

	@Override
	public String toString() {
		return "ReplyboardVO [no=" + no + ", title=" + title + ", writer=" + writer + ", content=" + content
				+ ", viewCnt=" + viewCnt + ", regDate=" + regDate + ", rootid=" + rootid + ", relevel=" + relevel + "]";
	}

}
