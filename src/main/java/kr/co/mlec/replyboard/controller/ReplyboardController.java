package kr.co.mlec.replyboard.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

//import kr.co.mlec.common.Pagination;
import kr.co.mlec.replyboard.service.ReplyboardService;
import kr.co.mlec.replyboard.vo.ReplyboardVO;

//@RequestMapping("/reply_board");

//xml에서 WEB-INF/jsp/ 햇으니까 경로가 이거로 시작해야 됨
@Controller
public class ReplyboardController {
	
	@Autowired
	private ReplyboardService replyservice;
	
	@RequestMapping("/replyBoard")
	public ModelAndView list(@RequestParam(required = false, defaultValue = "1") int page,
	         @RequestParam(required = false, defaultValue = "1") int range,
	         @RequestParam(required = false, defaultValue = "title") String searchType,
	         @RequestParam(required = false) String keyword, HttpServletRequest request, Model model,
	         HttpSession session) throws Exception {
		
//		Pagination pagination = new Pagination();
		
//		pagination.setKeyword(keyword);
//		pagination.setSearchType(searchType);
//
//		int listCnt = replyservice.selectBoardCnt(pagination);
//		//System.out.println("전체 게시물 개수: " + listCnt);
//
//	    pagination.pageInfo(page, range, listCnt);
	    
//		List<ReplyboardVO> replyboardList = replyservice.selectReplyboard(pagination);
		List<ReplyboardVO> replyboardList = replyservice.selectReplyboard();
		
		/*
		 * 1. forward시킬 jsp 주소 설정
		 * 2. jsp에서 출력할 게시물을 공유영역에 설정
		 */
		
		ModelAndView mav = new ModelAndView("replyBoard/inquiry");
//	    mav.addObject("listCnt", listCnt);
	    mav.addObject("replyboardList",replyboardList);
//	    mav.addObject("pagination2", pagination);
	    
		return mav;
	}
	
	@RequestMapping(value="/replyBoard/inquiry_write", method=RequestMethod.GET)
	public ModelAndView writeForm(Model model, @RequestParam("type") String type, @RequestParam("rootid") int rootid){
		
		model.addAttribute("replyboardVO", new ReplyboardVO());
		

		ModelAndView mav = new ModelAndView("replyBoard/inquiry_write");
		mav.addObject("type",type);
		mav.addObject("rootid",rootid);
		
		return mav;
	}
	
//	@RequestMapping(value="/replyBoard/inquiry_write", method=RequestMethod.POST)
	@PostMapping("/replyBoard/inquiry_write")
	public String write(@Valid ReplyboardVO replyboard, BindingResult result) {
		
		if(result.hasErrors()) {
			return "replyBoard/inquiry_write";
		}
		
		System.out.println(replyboard);
		replyservice.insertReplyboard(replyboard);
		int myNo = replyservice.selectMyNo();
		
		if (replyboard.getRelevel() == 1) {
			replyservice.updateRootid(replyboard.getRootid());
		} else {
			replyservice.updateRootid(myNo);
		}
		
		return "redirect:/replyBoard";
	}
	
//	상세 게시글 조회  /replyBoard/3
	@RequestMapping("/replyBoard/{no}")
	public ModelAndView detail(@PathVariable("no") int replyboardNo) {
		
		ReplyboardVO replyboard = replyservice.detailReplyboard(replyboardNo);
		replyservice.updateViewCnt(replyboardNo);
		
		ModelAndView mav = new ModelAndView("replyBoard/inquiry_detail");
		mav.addObject("replyboard", replyboard);
		
		return mav;
	}
	
	/*	
//	상세 게시글 조회  /replyBoard/list?no=3
	@RequestMapping("/replyBoard/inquiry_detail")
	public ModelAndView detail2(@RequestParam("no")int replyboardNo) {
		
		ReplyboardVO replyboard = replyservice.detailReplyboard(replyboardNo);
		
		ModelAndView mav = new ModelAndView("replyBoard/inquiry_detail");
		mav.addObject("replyboard", replyboard);
		
		return mav;
	}
	
	@PostMapping("/reply_board")
	public void insert(ReplyboardVO replyboard) {
		replyservice.insertReply(replyboard);
	}*/
	
	/*
	 * 게시글 수정 컨트롤러 /borad/guide_update/{no}
	 */
	
	@GetMapping("/replyBoard/inquiry_update/{no}")
	public ModelAndView updateForm(@PathVariable("no") int replyboardNo, Model model) {
		
		ReplyboardVO replyboardVO = replyservice.detailReplyboard(replyboardNo);

		model.addAttribute("replyboardVO", new ReplyboardVO());
		
		ModelAndView mav = new ModelAndView("/replyBoard/inquiry_update");
		mav.addObject("replyboardVO", replyboardVO);
		
		return mav;
	}
	
	@PostMapping("/replyBoard/inquiry_update")
	public String update(@Valid ReplyboardVO replyboardVO, BindingResult result) {
		
		if(result.hasErrors()) {
			return "/replyBoard/inquiry_update";
		}
		
		replyservice.updateReplyboard(replyboardVO);
		
		return "redirect:/replyBoard/"+replyboardVO.getNo();
	}
	
//	게시글 삭제
	@ResponseBody
//	@RequestMapping(value="/reply_board/{no}", method=RequestMethod.DELETE)
	@DeleteMapping("/replyBoard/{no}")
	public Map<String, String> delete(@PathVariable("no") int replyboardNo) {
		
		//글 지우기
		ReplyboardVO replyboard = replyservice.detailReplyboard(replyboardNo);

		System.out.print("no: "+replyboard.getNo());
		System.out.print("rootid: "+replyboard.getRootid());
		
		replyservice.deleteReplyboard(replyboardNo);
		
		if (replyboard.getNo() == replyboard.getRootid()) {
			//답글 지우기
			replyservice.deleteReply(replyboard.getRootid());
		}
		
		Map<String, String> map =  new HashMap<String, String>();
		map.put("msg", replyboardNo + "번 게시물이 삭제되었습니다");
		return map;
	}
	
	//
	
}


