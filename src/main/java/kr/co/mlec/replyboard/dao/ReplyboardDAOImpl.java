package kr.co.mlec.replyboard.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.board.vo.BoardVO;
//import kr.co.mlec.common.Pagination;
import kr.co.mlec.replyboard.vo.ReplyboardVO;
/**
 * mybatis를 통한 oracle DB 게시판 CRUD 구현클래스
 * @author 401Prof
 *
 */
@Repository
public class ReplyboardDAOImpl implements ReplyboardDAO {

	@Autowired
	private SqlSessionTemplate session;
	
//	public List<ReplyboardVO> select(Pagination pagination) {
//		List<ReplyboardVO> list = session.selectList("replyboard.dao.ReplyboardDAO.selectAll", pagination);
//		
//		return list;
//	}
	
	public List<ReplyboardVO> select() {
		List<ReplyboardVO> list = session.selectList("replyboard.dao.ReplyboardDAO.selectAll");
		
		return list;
	}
	
//	public int selectBoardCnt(Pagination pagination) {
//		int cnt = session.selectOne("replyboard.dao.ReplyboardDAO.selectBoardCnt", pagination);
//		return cnt;
//	}


	public void insert(ReplyboardVO replyboard) {
		
		session.insert("replyboard.dao.ReplyboardDAO.insert", replyboard);
	}
	
	public int selectMyNo() {
		int myNo = session.selectOne("replyboard.dao.ReplyboardDAO.selectMyNo");
		return myNo;
	}

	public void updateRootid(int rootid) {
		
		session.update("replyboard.dao.ReplyboardDAO.updateRootid", rootid);
	}

	public ReplyboardVO selectByNo(int replyboard) {
		ReplyboardVO replyboardVO = session.selectOne("replyboard.dao.ReplyboardDAO.selectByNo", replyboard);
		return replyboardVO;
	}
	
	public void updateViewCnt(int replyboardNo) {
		session.delete("replyboard.dao.ReplyboardDAO.updateViewCnt", replyboardNo);
	}

	public void updateByNo(ReplyboardVO replyboardVO) {
		session.update("replyboard.dao.ReplyboardDAO.updateByNo", replyboardVO);
	}
	
	public void deleteByNo(int replyboardNo) {
		session.delete("replyboard.dao.ReplyboardDAO.deleteByNo", replyboardNo);
	}

	public void deleteReply(int rootid) {
		session.delete("replyboard.dao.ReplyboardDAO.deleteReply", rootid);
	}

}


