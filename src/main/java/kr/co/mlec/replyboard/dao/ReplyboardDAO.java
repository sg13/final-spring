package kr.co.mlec.replyboard.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.mlec.board.vo.BoardVO;
//import kr.co.mlec.common.Pagination;
import kr.co.mlec.replyboard.vo.ReplyboardVO;

/**
 * 게시판 데이터 CURD를 위한 기능클래스
 * @author 401Prof
 *
 */
@Repository
public interface ReplyboardDAO {

	/**
	 * 전체게시글 조회 서비스
	 * @return 조회된 게시물
	 */
//	List<ReplyboardVO> select(Pagination pagination);
	List<ReplyboardVO> select();
	
	/*
	 * 전체게시글 수 
	 * @return 전체 게시글 수
	 */
//	int selectBoardCnt(Pagination pagination);
	
	/**
	 * 새글등록 서비스
	 * @param replyboard 등록할 게시물 정보
	 */
	void insert(ReplyboardVO replyboard);
	
	/**
	 * 상세게시글 조회 서비스
	 * @param replyboardNo 조회할 글번호
	 * @return 조회된 게시물
	 */
	int selectMyNo();
	
	/**
	 * rootid 증가 서비스
	 * @param replyboardNo rootid를 증가시킬 글 번호
	 */
	void updateRootid(int replyboardNo);

	/**
	 * 상세게시글 조회 서비스
	 * @param replyboardNo 조회할 글번호
	 * @return 조회된 게시물
	 */
	ReplyboardVO selectByNo(int replyboardNo);
	
	/**
	 * 조회수 증가 서비스
	 * @param boardNo 조회할 글번호
	 */
	void updateViewCnt(int replyboardNo);

	/**
	 * 해당게시글 수정 서비스
	 * @param board 수정할 글 내용
	 */
	void updateByNo(ReplyboardVO replyboardVO);	
	
	/**
	 * 해당게시글 삭제 서비스
	 * @param replyboardNo 삭제할 글번호
	 */
	
	void deleteByNo(int replyboardNo);
	/**
	 * 답글 삭제 서비스
	 * @param rootid 삭제할 글 rootid
	 */
	void deleteReply(int rootid);
	
}










