package kr.co.mlec.replyboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.board.vo.BoardVO;
//import kr.co.mlec.common.Pagination;
import kr.co.mlec.replyboard.dao.ReplyboardDAO;
import kr.co.mlec.replyboard.vo.ReplyboardVO;

@Service
public class ReplyboardServiceImpl implements ReplyboardService {

	@Autowired
	private ReplyboardDAO replyboardDAO;
	
//	public List<ReplyboardVO> selectReplyboard(Pagination pagination) {
//		List<ReplyboardVO> replyboardList = replyboardDAO.select(pagination);
//		return replyboardList;
//	}
	public List<ReplyboardVO> selectReplyboard() {
		List<ReplyboardVO> replyboardList = replyboardDAO.select();
		return replyboardList;
	}

//	public int selectBoardCnt(Pagination pagination) {
//		int cnt = replyboardDAO.selectBoardCnt(pagination);
//		return cnt;
//	}
	
	public void insertReplyboard(ReplyboardVO replyboard) {
		replyboardDAO.insert(replyboard);
	}

	public int selectMyNo() {
		int myNo = replyboardDAO.selectMyNo();
		return myNo;
	}

	public void updateRootid(int replyboardNo){
		replyboardDAO.updateRootid(replyboardNo);
	}
	
	public ReplyboardVO detailReplyboard(int replyboardNo) {
		return replyboardDAO.selectByNo(replyboardNo);
	}

	public void updateViewCnt(int replyboardNo) {
		replyboardDAO.updateViewCnt(replyboardNo);
	}

	public void updateReplyboard(ReplyboardVO replyboardVO) {
		replyboardDAO.updateByNo(replyboardVO);
	}
	
	public void deleteReplyboard(int replyboardNo) {
		replyboardDAO.deleteByNo(replyboardNo);
	}
	
	public void deleteReply(int rootid) {
		replyboardDAO.deleteReply(rootid);
	}
	
}











