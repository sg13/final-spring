package kr.co.mlec.replyboard.service;

import java.util.List;

//import kr.co.mlec.common.Pagination;
import kr.co.mlec.replyboard.vo.ReplyboardVO;

public interface ReplyboardService {

//	List<ReplyboardVO> selectReplyboard(Pagination pagination);
//	int selectBoardCnt(Pagination pagination);
	List<ReplyboardVO> selectReplyboard();
//	
	void insertReplyboard(ReplyboardVO replyboardVO);
	int selectMyNo();
	void updateRootid(int replyboardNo);
	
	ReplyboardVO detailReplyboard(int boardNo);
	void updateViewCnt(int replyboardNo);
	
	void updateReplyboard(ReplyboardVO replyboardVO);
	
	void deleteReplyboard(int replyboardNo);
	void deleteReply(int rootid);
}