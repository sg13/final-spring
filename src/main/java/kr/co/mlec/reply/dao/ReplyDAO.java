package kr.co.mlec.reply.dao;

import java.util.List;

import kr.co.mlec.reply.vo.ReplyVO;

public interface ReplyDAO {

	void insert(ReplyVO replyVO);

	List<ReplyVO> select(int no);
}
