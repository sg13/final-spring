package kr.co.mlec.reply.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.board.vo.BoardVO;
import kr.co.mlec.reply.vo.ReplyVO;

@Repository
public class ReplyDAOImpl implements ReplyDAO {

	@Autowired
	private SqlSessionTemplate session;
	
	public void insert(ReplyVO replyVO) {
		System.out.println("DAO");
		System.out.println(replyVO);
		session.insert("reply.dao.ReplyDAO.insert", replyVO);
	}

	@Override
	public List<ReplyVO> select(int boardNo) {
		List<ReplyVO> list = session.selectList("reply.dao.ReplyDAO.selectAll", boardNo);
		return list;
	}

}
