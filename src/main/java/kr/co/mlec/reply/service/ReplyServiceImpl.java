package kr.co.mlec.reply.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.board.vo.BoardVO;
import kr.co.mlec.reply.dao.ReplyDAO;
import kr.co.mlec.reply.vo.ReplyVO;

@Service
public class ReplyServiceImpl implements ReplyService {

	@Autowired
	private ReplyDAO replyDAO;
	
	public void insertReply(ReplyVO replyVO) {
		
		System.out.println("service");
		System.out.println(replyVO);
		replyDAO.insert(replyVO);
	}

	@Override
	public List<ReplyVO> selectReply(int no) {
		List<ReplyVO> replyList = replyDAO.select(no); 
		return replyList;
	}

}
