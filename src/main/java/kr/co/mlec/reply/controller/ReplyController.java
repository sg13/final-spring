package kr.co.mlec.reply.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import kr.co.mlec.p2p.vo.P2PVO;
import kr.co.mlec.reply.service.ReplyService;
import kr.co.mlec.reply.vo.ReplyVO;

@RestController
public class ReplyController {
	
	@Autowired
	private ReplyService replyService;

	@PostMapping("/reply")
	public void insert(ReplyVO replyVO) {
		
		System.out.println("Controller");
		System.out.println(replyVO);
		replyService.insertReply(replyVO);
	}
	
	@ResponseBody
	@GetMapping("/reply/{boardNo}")
	public Map<String, Object> select(@PathVariable("boardNo") int boardNo) {
		
		List<ReplyVO> reply = replyService.selectReply(boardNo);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("reply", reply);
		
//		@responseBody가 있기 때문에 map을 return 하긴 하지만 map형태가 아니라 json 형태이다
		return map;
	}
}
