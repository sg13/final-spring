package kr.co.mlec.account.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.mlec.account.service.AccountService;
import kr.co.mlec.account.vo.AccountVO;
import kr.co.mlec.member.service.MemberService;
import kr.co.mlec.member.vo.MemberVO;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private MemberService memberService;
	
	String[] depositorList = {"원숭이", "멍멍이", "고양이", "말"};

 @ResponseBody
 @PostMapping("/checkAccount")
 public Map<String, Object> accountCheck(@RequestParam("account") int account,
								 	     @RequestParam("bank") String bank,
								 	     HttpServletRequest request,
								 	     AccountVO accountVO) {
	
	HttpSession session = request.getSession();
	MemberVO userVO = (MemberVO) session.getAttribute("userVO");
	String custId = userVO.getId();
	String name = memberService.selectName(custId);
	
//	depositor값 update
	int random = (int)Math.floor(Math.random()*4);
	String depositor = depositorList[random].toString();
	
	System.out.println("account" + account);
	System.out.println("bank" + bank);
	System.out.println("name" + name);
	System.out.println("depositor" + depositor);
	
	accountVO.setAccount(account);
	accountVO.setBank(bank);
	accountVO.setName(name);
	accountVO.setDepositor(depositor);
	System.out.println("test1");
	
	System.out.println(accountVO.toString());
	
	accountService.checkAccount(accountVO);
	System.out.println("update가 잘 됐나...");
	System.out.println(accountService.checkAccount(accountVO));
	System.out.println("test2");
	
	Map<String, Object> map = new HashMap<String, Object>();
	System.out.println("test3");

	int result = accountService.checkAccount(accountVO);
	
	if(result == 0) {
		map.put("result", 0);
	} else {
		map.put("result", depositor);
	}
	
	System.out.println("test4");
	
	return map;
 }
 
 @ResponseBody
 @PostMapping("/addAccount")
	public Map<String, Object> addAccount( @RequestParam("account") int account, 
										   @RequestParam("bank") String bank,
										   @RequestParam("type") String type,
										   HttpServletRequest request,
										   AccountVO accountVO) {
		
		
		HttpSession session = request.getSession();
		MemberVO userVO = (MemberVO) session.getAttribute("userVO");
		String custId = userVO.getId();
		String name = memberService.selectName(custId);
		
		System.out.println("custId : " + custId);
		
	 	accountVO.setAccount(account);
		accountVO.setBank(bank);
		accountVO.setType(type);
		accountVO.setCustId(custId);
		accountVO.setName(name);
		System.out.println("세팅 완료");
		System.out.println(accountVO);
		
		int balance = accountService.selectBalance(accountVO);
		accountVO.setBalance(balance);
		System.out.println(accountVO);
		
		accountService.addAccount(accountVO);
		System.out.println("insert 완료");
	 
		Map<String, Object> map = new HashMap<String, Object>();
		
//		key값을 calcList로 calcList 자체를 map에 저장
		map.put("key", "계좌 등록에 성공하였습니다.");
		
		return map;
	}
 
 
 
}
