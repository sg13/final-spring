package kr.co.mlec.account.dao;

import java.util.List;

import kr.co.mlec.account.vo.AccountVO;

public interface AccountDAO {


	List<AccountVO> select(String id);

	void updateBalance(AccountVO accountVO);

	int checkAccount(AccountVO accountVO);

	void addAccount(AccountVO accountVO);

	int selectBalance(AccountVO accountVO);

}
