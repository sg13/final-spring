package kr.co.mlec.account.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.mlec.account.vo.AccountVO;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	SqlSessionTemplate session;
	
	@Override
	public List<AccountVO> select(String id) {
		List<AccountVO> accountList = session.selectList("account.dao.AccountDAO.select", id);
		return accountList;
	}

	@Override
	public void updateBalance(AccountVO accountVO) {
		session.update("account.dao.AccountDAO.updateBalance", accountVO);
	}

	@Override
	public int checkAccount(AccountVO accountVO) {
		System.out.println("여기는 DAO");
		System.out.println(accountVO.toString());
		int success = session.update("account.dao.AccountDAO.checkAccount", accountVO);
		System.out.println(success);
		return success;
	}

	@Override
	public void addAccount(AccountVO accountVO) {
		session.insert("account.dao.AccountDAO.addAccount", accountVO);
		
	}

	@Override
	public int selectBalance(AccountVO accountVO) {
		System.out.println("여기는 DAO");
		System.out.println(accountVO.toString());
		int balance = session.selectOne("account.dao.AccountDAO.selectBalance", accountVO);
		System.out.println(balance);
		return balance;
	}

}
