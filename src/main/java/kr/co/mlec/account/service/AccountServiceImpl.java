package kr.co.mlec.account.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.mlec.account.dao.AccountDAO;
import kr.co.mlec.account.vo.AccountVO;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountDAO accountDAO;
	
	@Override
	public List<AccountVO> select(String id) {
		List<AccountVO> accountList = accountDAO.select(id);
		return accountList;
	}

	@Override
	public void updateBalance(AccountVO accountVO) {
		accountDAO.updateBalance(accountVO);
	}

	@Override
	public int checkAccount(AccountVO accountVO) {
		System.out.println("여기는 service");
		System.out.println(accountVO.toString());
		int success = accountDAO.checkAccount(accountVO);
		System.out.println(success);
		return success;
	}

	@Override
	public void addAccount(AccountVO accountVO) {
		accountDAO.addAccount(accountVO);
	}

	@Override
	public int selectBalance(AccountVO accountVO) {
		System.out.println("여기는 Service");
		System.out.println(accountVO.toString());
		
		int balance = accountDAO.selectBalance(accountVO);
		System.out.println(balance);
		return balance;
	}

}
