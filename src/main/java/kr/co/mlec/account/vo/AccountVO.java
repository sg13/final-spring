
package kr.co.mlec.account.vo;

import org.springframework.stereotype.Component;

@Component
public class AccountVO {

	private String custId;
	private String bank;
	private int account;
	private int balance;
	private int investAmount;
	private String type;
	private String name;
	private String depositor;
	
	public AccountVO(String custId, String bank, int account, int balance, String type, int investAmount, String name, String depositor) {
		super();
		this.custId = custId;
		this.bank = bank;
		this.account = account;
		this.balance = balance;
		this.type = type;
		this.investAmount = investAmount;
		this.name = name;
		this.depositor = depositor;
	}

	public AccountVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public int getAccount() {
		return account;
	}

	public void setAccount(int account) {
		this.account = account;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getInvestAmount() {
		return investAmount;
	}

	public void setInvestAmount(int investAmount) {
		this.investAmount = investAmount;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}
	
	@Override
	public String toString() {
		return "AccountVO [custId=" + custId + ", bank=" + bank + ", account=" + account + ", balance=" + balance
				+ ", type=" + type + ", investAmount=" + investAmount + ", name=" + name + ", depositor=" + depositor +"]";
	}
	
}
