package kr.co.mlec.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.co.mlec.member.vo.MemberVO;

//이전에는 id가 없으면 페이지 마다 if문 해서 설정했다면 얘는 일괄적으로 한 번에!
@Service
public class LoginInterceptor extends HandlerInterceptorAdapter {

//	client가 요청하는 모든 정보들 거기에서 set attribute라고 하면 request 영역에 저장하는 것
//	만약 session영역에 하고 싶을 땐 request.getsession이라고 하는 것
//	클라이언트에게 응답하기 위해 response 객체 생성하는 것
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler)
			throws Exception {
		
		/*
		HandlerMethod method = (HandlerMethod)handler;
		
		System.out.println("Controller : " + method.getBean());
		System.out.println("method : " + method);
		*/

		HttpSession session = request.getSession();
		MemberVO userVO = (MemberVO)session.getAttribute("userVO");
		
		if(userVO == null) {
			
			response.sendRedirect(request.getContextPath() + "/login");
			
			return false;
		} 		
		return true;
	}

	
}














