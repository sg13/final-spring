//리워드 funda.js?v2=1565177857

$(document).ready(function(){
	
    var deposit = -1;
    var unit = 1; //단위 입력(만원)
    
    var roi = $('#roi').val();
    var max_amount = $('#totalGoal').val();
    var merchandiseId = $('#투자하기').data("id");
    var p2pNo;
    var period;
 
    $('#amount').change(function () {	//1. 투자금액 입력에 관한 유효성체크
        var value = $(this).val();
        var flag = !checkInvestAmount(value, max_amount, unit);	//값이 올바르면 true
        $('#상환일정보기').attr("disabled", flag);
    });
    
    $('#amount').keyup(function () {
    	
    	alert(roi);
    	alert(max_amount);
    	
        var value = $(this).val() * 10000;
        var flag = !checkValidInvest(value, max_amount);

        $('#상환일정보기').attr("disabled", flag);
    });
    
    $('.add-amount input').click(function () {	//2. 금액 증가 버튼을 눌렀을때
        var value = parseInt($('#amount').val() == "" ? 0 : $('#amount').val());
        var add = parseInt($(this).attr("id"));
        var add_value = add == 0 ? "" : value + add;
        $('#amount, .amount_invest').val(add_value);

        var flag = !checkInvestAmount(add_value, max_amount, unit);
        $('#상환일정보기').attr("disabled", flag);
        if (add == 0) {
            $('#amount-msg').addClass("visibility-hidden");
        }
    });

    $('#상환일정보기').click(function () {		//3. 상환일정 보기
    	var value = parseInt($('#amount').val() || 0);
    	console.log("value: ");
    	console.log(value);
        
    	if (value == "" || isNaN(value)) {
            alert('금액을 입력해주세요');
            return;
        } else if (value < 1) {
            alert('1만원 이상 입력해주세요');
            return;
        }
    	
    	period = $('#loan_period'+merchandiseId).val();
    	console.log("period: ");
    	console.log(period);
    	
        	$.ajax({
    			url: "/p2p",
    			type: "post",
    			data: {roi:roi, period:period, value:value*10000},
    			success : function(data){

    				var list = JSON.parse(data);
    				var calcList = list.calcList;
    				console.log(calcList);
    				
    				var totalRepayMonth = 0;//상환원금 합
    		        var totalInterestRepay = 0;//세전이자 합
    		        var totalTax = 0;//세금 합
    		        var totalAfterTax = 0;//세후 합계 합
		            
		            $('#scheduleTable > tbody').empty();
		            
    				console.log(calcList.length);
    				for(var i = 0; i < calcList.length; i++){
		               addRow = "<tr>";
		               addRow += "<th>"+ calcList[i].index+ "회차</th>";
		               addRow += "<td>"+ calcList[i].originRepay+ "원</td>";
		               addRow += "<td>"+ calcList[i].interestRepay+ "원</td>";
		               addRow += "<td>"+ calcList[i].tax+ "원</td>";
		               addRow += "<td>"+ calcList[i].afterTax+ "원</td>";
		               addRow += "</tr>";
		               
		               totalRepayMonth += calcList[i].originRepay;//상환원금 합
	    		       totalInterestRepay += calcList[i].interestRepay;//세전이자 합
	    		       totalTax += calcList[i].interestRepay;//세금 합
	    		       totalAfterTax += calcList[i].afterTax;//세후합계 합
		               
		               $('#scheduleTable > tbody:last').append(addRow);
    				}
    				
    					addRow = "<tr>";
    					addRow += "<th>합계</th>";
    					addRow += "<th>" + totalRepayMonth + "원</th>";
    					addRow += "<th>" + totalInterestRepay + "원</th>";
    					addRow += "<th>" + totalTax + "원</th>";
    					addRow += "<th>" + totalAfterTax + "원</th>";
    					addRow += "</tr>";
    					
    					$('#scheduleTable > tbody:last').append(addRow);
    					$('#scheduleTable').css("display", "block");
    					addRow = null;
    			}
    		});
    });
    
    $('#투자하기').click(function () {
        if ($('#custId_invest').val() == "") {
            alert('로그인이 필요합니다.');
            location.href = "/login";
            return;
        }
        
        $.ajax
    	var merchandiseId = $(this).data("id");
        var value = parseInt($('#amount').val() || 0);
        if (value > max_amount) {
            alert('최대투자가능금액을 초과하셨습니다');
            $('#amount').val("");
        } else if (value < 1) {
            alert('1만원 이상 투자가능합니다');
            $('#amount').val("");
        } else {
            value *= 10000;
            p2pNo = $('#merchandise-idx'+merchandiseId).val();
            period = $('#loan_period'+merchandiseId).val();
            investBtn(p2pNo, period, value);
        }
    });
    
//  최종 등록 버튼
    var account = $('#modal_account').val();
	var bank = $('#modal_bank').val();
	var type = $('input[name="paytype"]:checked').val();
	var acount_result; 
    
//	$('#accountAdd').click(function () {
//        
//    	if (account == "" || isNaN(account)) {
//            alert('계좌를 입력해주세요');
//            return;
//        }
//	});
	
//	인증 버튼
	$('#account_veri_btn').click(function(){
		
		$.ajax({
			url: "/checkAccount",
			type: "post",
			data: {"account":$('#modal_account').val(), "bank":$('#modal_bank').val()},
			success : function(data){
				account_result = JSON.parse(data).result;
				
				if(account_result == 0){
					alert("조회되는 계좌가 없습니다.");
					$("#accountCheck").hide();
					$('#accountAdd_modal').on('hidden.bs.modal',function(){
						$(this).removeData();
					});
					return false;
				} else {
					$('#hidden_depositor').val(account_result);
					$("#accountCheck").show();
					return true;
				}
				
			}
		})
	});
    	
	
	$('#accountAdd').click(function(){
		var inputdepositor = $('#modal_depositor').val();
		
		var hiddendepositor = $('#hidden_depositor').val();
		
		if (inputdepositor == "" || inputdepositor != hiddendepositor) {
			$('#depositorErrMsg').show();
			return false;
			
		} else if(inputdepositor == hiddendepositor){
			
			$('#depositorErrMsg').hide();
			$.ajax({
				url: "/addAccount",
				type: "post",
				data: {"account":$('#modal_account').val(),
					   "bank":$('#modal_bank').val(),
					   "type":$('input[name="payType"]:checked').val()
				},
				success : function(data){
					var success = JSON.parse(data).key;
					alert(success);
					
					$('#accountAdd_modal').on('hidden.bs.modal',function(){
						$(this).removeData();
					});
				}
			})
		}
	});
	
	/*오류*/
	$('#simpPwd').blur(function(){
		checkSimpPwd();
	});
	$('#agreeBox').blur(function(){
		agreement();
	});
	
	$('#customP2P').click(function () {		//3. 상환일정 보기
    	var custId = $('#custIdP2P').val();
    	$('#customP2PtableDiv').show();
    	$('#customP2Ptable').show();
        	$.ajax({
    			url: "/custP2P",
    			type: "post",
    			data: {id:custId},
    			success : function(data){
    				var list = JSON.parse(data);
    				
    				var custP2PList = list.custP2PList;
    				console.log(custP2PList);
		            
		            $('#customP2Ptable > tbody').empty();
		            
    				console.log(custP2PList.length);
    				for(var i = 0; i < custP2PList.length; i++){
		               addRow = "<tr>";
		               addRow += "<td>"+ custP2PList[i].p2pNo + "호</td>";
		               addRow += "<td>"+ custP2PList[i].investAmount + "원</td>";
		               addRow += "<td>"+ custP2PList[i].sold_date + "</td>";
		               addRow += "<td>"+ custP2PList[i].due_date + "</td>";
		               addRow += "</tr>";
		               
		               $('#customP2Ptable > tbody:last').append(addRow);
		               addRow = null;
    				}
    			}
    		});
    });
	
	

});

function investBtn(p2pNo, period, value){
	   
   var url = "/invest/order";
   url = url + "?p2pNo=" + p2pNo;
   url = url + "&period=" + period;
   url = url + "&value=" + value;
   location.href = url;
   $('#invAmount').val(value);
}

//금액을 10만원 단위로 조정
function setTenUnit(id, unit) {
	var value = $('#'+id).val();
	var unit = Number(unit);
	if (value < unit) 	return;			//10만원 보다 작은 금액은 리턴
	if (value%unit != 0) {				//10만원 단위가 아니라면 경고창
		$('#'+id).val('');
		return false;		//10만원단위가 아니면 false
	} else {
		return true;
	}
}

function checkInvestAmount(value, max_amount, unit) {
	var flag = false;
	if (!setTenUnit('amount', unit)) {
		$('#amount-msg').html(unit + "만원단위로 입력하세요");
		$('#amount-msg').removeClass("display-none");
	} else if (value > max_amount) {
		$('#amount').val('');
		$('#amount-msg').html("*최대 투자가능금액을 초과하셨습니다");
		alert("*최대 투자가능금액을 초과하셨습니다");
		$('#amount-msg').removeClass("display-none");
	} else {
		$('#amount-msg').html("-");
		$('#amount-msg').addClass("display-none");
		$(this).val(parseInt(value));
		flag = true;
	}
	return flag;
}

function checkValidInvest(value, max_amount) {
	var flag = false;
	if ( (value%10 == 0) && (10 <= value && value <= max_amount) ) {
		flag = true;
	}
	return flag;
}

function deleteP2PBtn(){
	var p2pNo = $('#merchandise-idx').val();
	if(confirm(p2pNo + '번 상품을 삭제하시겠습니까?')){
		$.ajax({
			url:'/p2p/'+ p2pNo,
			type: 'delete',
			success: function(data){
				var data = JSON.parse(data).msg;
				alert(data);
				
				location.href="/p2p";
			}
		});
	}
}

function drawChart(){
	
	$.ajax({
		url:'/p2pChart',
		type: 'get',
		data: JSON.stringify(postData),
	    contentType: "application/json; charset=utf-8",
		success: function (data) {
//			data를 json으로 parsing
	         var chart = JSON.parse(data);
	         
//			key값으로 해당 배열들 저장
	         var generationChart = chart.chart1_1;
//	         var amountTop3List = chart.chart1_2;
//	         var genderChart = chart.chart2;
//	         var generationChart = chart.chart3;
	         
//	                각 배열을 매개변수로 하는 googleChart함수 실행
//	         googleChart(countTop3List, amountTop3List
//	        		 	 , genderChart, generationChart);
	         googleChart(generationChart);
      }
   })
}

//function googleChart(countTop3List, amountTop3List
//					 , genderChart, generationChart){
function googleChart(generationChart){
   
    var generationChart_data = new google.visualization.DataTable();

//  세대 비율 파이 차트를 위한 '테이블' 컬럼 생성
    generationChart_data.addColumn('string', 'Generation');
    generationChart_data.addColumn('number', 'Generation_ratio');
    
//	chart3에 저장된 배열 길이 만큼 반복문 돌리면서 해당 값들을
//    		위에서 만든 테이블에 insert
    for(var i = 0; i < generationChart.length; i++){
//    	generationChart의 0번째부터 마지막 인덱스 값의 key값!
//    	즉, 맵 배열 안에 있는 맵의 key 값!
//    	세대   | 비율
//    	--------
//    	10대 | 20
//    	20대 | 31
    	generationChart_data.addRows([
          [generationChart[i].GENERATION, generationChart[i].GENERATION_RATIO]
       ])
    };
    
    var generationChart_options = {
    	   'title' : '투자자 연령대 비율',
	       'fontSize': 20,
	       'width' :800,
	       'height' : 500
	};
    
    var generationChart = new google.visualization.PieChart(document.getElementById('generationChart'));
    
    generationChart.draw(generationChart_data, generationChart_options);
}

function drawVisualization(){

	var options1 = {
			title : '투자 거래량 Top3 상품',
			vAxis: {title: '거래량'},
			hAxis: {title: '투자 상품 번호'}, 
			seriesType: 'bars',
			series: {1: {type: 'line'}}
	};
	
	$.ajax({
		url:'/p2pChart',
		type: 'get',
		success: function (data) {
			
			var data1 = google.visualization.arrayToDataTable(chart1_1);
			var data2 = google.visualization.arrayToDataTable(chart1_2);
			
			var options2 = {
					title : '투자 거래액 Top3 상품',
					vAxis: {title: '거래액'},
					hAxis: {title: '투자 상품 번호'}, 
					seriesType: 'bars',
					series: {1: {type: 'line'}}
			};
			
			var chart = new google.visualization.ComboChart(document.getElementById('p2pChart'));
			chart.draw(data1, options1);
			chart.draw(data2, options2);
		}
	})
}



/*에러 메세지 함수*/
function checkSimpPwd(){
	if($('#simpPwd').val() != $('#hiddenSimpPwd').val()){
		$('#simpPwdErrMsg').show();
		return false;
	} else {
		$('#simpPwdErrMsg').hide();
		return true;
	}
}

function agreement(){
	if($('#agreeBox').val() != '동의'){
		$('#agreementErrMsg').show();
		return false;
	} else {
		$('#agreementErrMsg').hide();
		return true;
	}
}
