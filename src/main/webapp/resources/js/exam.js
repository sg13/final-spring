var trueCheck=[false,false,false,false,false,false,false];
var idList=[];

$(document).ready(function(){
  $("#loginButton").click(function(){
    if(key.keyCode == 13){
      $("login_form").submit();
    }
  });
  $("#logoutButton").click(function(){
    location.href="exam_logout.php";
  });
  $("#modifyButton").click(function(){
    location.href="exam.html";
  });
  $("#applyNewButton").click(function(){
    location.href="exam.html";
  });
  $("#home").click(function(){
    location.href="main.html";
  });
  //----회원가입 form 함수들---//
  $("#userId").blur(function(){
    $.checkID();
  });
  $("#userPassword").blur(function(){
    $.checkPasswordLength();
  });
  $("#userPassCheck").blur(function(){
    $.checkPassword();
  });
  $("#userName").blur(function(){
    $.checkName();
  });
  $("#birth").blur(function(){
    $.checkBirth();
  });
  $("#userEmail").blur(function(){
    $.checkEmail();
  });
  $("#userPhone").blur(function(){
    $.checkPhoneNumber();
  });
  $("#verifyBtn").click(function(){
    $.getverifyCode();
  });
  $("#verifyInput").blur(function(){
    $.codeCheck();
  });
  $("#joinsubmitBtn").click(function(){
    $.join();
  });
  //---------------------------------//
  $("#modifysubmitBtn").click(function(){
    $.modify();
  });
  $(function(){
    $("#birth").datepicker({
      changeYear: true,
      changeMonth: true,
      nextText: '다음 달',
      prevText: '이전 달',
      dateFormat: "yymmdd",
      dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],
      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
      monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
      monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
      showMonthAfterYear:true,
      yearSuffix: '년',
      yearRange: '-100:+0'
    });
  });
});

$.checkID = function(){
  var idStr = $("#userId").val();
  idList[0] = "userId";
  if(idStr.length<4){
    $("#userDuplicateError").hide();
    $("#userIDerrorText").show();
    $("#userIDscss").hide();
    trueCheck[0] = false;
  }else{//이제 네 글자 이상일 때 중복 ID 체크를 해야겠지

    // GET 방식으로 서버에 HTTP Request를 보냄.
    $.get("exam_duplicateID.php",
      { userId : idStr}, // 서버가 필요한 정보를 같이 보냄.
                        //exam_duplicateID.php의 $userId와 idStr을 '비교한다'
        function(data, status) {//전송받은 데이터와 전송성공 여부를 보여줌
          //data.변수명으로 하면 접근이 바로 가능해짐
          //즉, php에서 check_result에 false값을 대입시킴. false인 경우는 중복ID가 있는 경우!
          if(data.check_result == 'false'){
            $("#userDuplicateError").show();
            $("#userIDerrorText").hide();
            $("#userIDscss").hide();
            // $("#userDuplicateError").html(data + "<br>" + status);
            trueCheck[0] = false;
            //$("#text").html(data + "<br>" + status); // 전송받은 데이터와 전송 성공 여부를 보여줌.
        } else {
            $("#userIDerrorText").hide();
            $("#userDuplicateError").hide();
            $("#userIDscss").show();
            trueCheck[0] = true;
        }
      }
    );
   }
}

//비번 길이 체크
$.checkPasswordLength = function(){
  var passStr = $("#userPassword").val();
  idList[1] = "userPassword";
  if(passStr.length<4){
    $("#passwordErrorText").show();
    trueCheck[1] = false;
  }else{
    $("#passwordErrorText").hide();
    trueCheck[1] = true;
  }
}
//비번 확인 체크
$.checkPassword = function(){
  var password = $("#userPassword").val();
  var passwordCheck = $("#userPassCheck").val();
  idList[2] = "userPassCheck";
  if(password != passwordCheck){
    $("#passwordCheckError").show();
    trueCheck[2] = false;
  }else{
    $("#passwordCheckError").hide();
    trueCheck[2] = true;
  }
}
//이름은 길이가 0이 아닐 때만
$.checkName = function(){
  var nameStr = $("#userName").val();
  idList[3] = "userName";
  if(nameStr.length < 1){
    $("#nameErrorText").show();
    trueCheck[3] = false;
  }else{
    $("#nameErrorText").hide();
    trueCheck[3] = true;
  }
}
$.checkBirth = function(){
  var birthStr = $("#birth").val();
  idList[4] = "birth";
  if(birthStr.length < 1){
    $("#birthCheckError").show();
    trueCheck[4] = false;
  } else{
    $("#birthCheckError").hide();
    trueCheck[4] = true;
  }
}
$.checkEmail = function(){
  var emailStr = $("#userEmail").val();
  idList[5] = "userEmail";
  if(emailStr.length < 1){
    $("#emailErrorText").show();
    trueCheck[5] = false;
  }else{
    $("#emailErrorText").hide();
    trueCheck[5] = true;
  }
}

$.checkPhoneNumber = function(){
  var phoneStr = $("#userPhone").val();
  idList[6] = "userPhone";
  if(phoneStr.length<1){
    $("#phoneNumErrorText").show();
    trueCheck[6] = false;
    return false;
  } else{
    $("#phoneNumErrorText").hide();
    trueCheck[6] = true;
    return true;
  }
}

//인증번호는 전역변수로 해야 다른 함수에서도 사용 가능
var verifyCode=0;
$.getverifyCode = function(){
  if($.checkPhoneNumber()){
    var result=Math.floor(Math.random()*1000)+1;
    alert("인증번호 : " + result);
    verifyCode = result;
    $("#verifyInput").css("background-color", "white");
    $("#verifyInput").attr("disabled",false);
    $("#verifyInput").focus();
  }
}
$.codeCheck = function(){
  var inputCode = $("#verifyInput").val();
  idList[7] = "verifyInput";
  if(inputCode != verifyCode){
    $("#verifyInputErrorText").show();
    trueCheck[7] = false;
  } else if(!inputCode){
    $("#verifyInputErrorText").show();
    trueCheck[7] = false;
  } else {
    $("#verifyInputErrorText").hide();
    trueCheck[7] = true;
  }
}
$.join = function(){
  var count = 0;
  for(i = 0; i <= 7; i++){
    if(trueCheck[i] == false) {
      $(idList[i]).focus();
      break;
    } else {
      count++;
    }
  }
  // true값 '개수'만큼
  if(count >= 8){
    // location.href="exam.php";
    $("#member_form").submit();
  } else {
    $.doublecheck();
    // for(i = 0; i < trueCheck.length; i++){
    //   alert(i + "번째" + trueCheck[i]);
    // }
  }
}

//제출버튼 시 빈칸인 경우를 대비해서
$.doublecheck = function(){
  $.checkID();
  $.checkPasswordLength();
  $.checkPassword();
  $.checkName();
  $.checkBirth();
  $.checkEmail();
  $.checkPhoneNumber();
  $.codeCheck();
}

$.modify = function(){
  //blur없이 따로 함수를 실행하는 이유는 수정시엔 굳이 form을 건드리지 않고 기존 값으로도 오류가 나선 안되기에
  $.checkPasswordLength();
  $.checkPassword();
  $.checkName();
  $.checkBirth();
  $.checkEmail();
  $.checkPhoneNumber();
  $.codeCheck();

  var count = 0;
  for(i = 1; i <= 7; i++){
    if(trueCheck[i] == false) {
      $(idList[i]).focus();
      break;
    } else {
      count++;
    }
  }
  //내가 했던 실수가 뭐냐면
  //i를 count로 씀. 그러다보니 마지막 코드 check할 땐 그 전 반복문에서 count + 1,
  //즉, 6+1=7이 된 상태로 마지막 loop를 돎. 이때는 false여도 마지막이기에 반복구문 빠져나옴
  //마지막 코드 check, 즉, trueCheck[7]=false여도 count=7까지 된 상태니까 submit이 되는 것임!
  //전역변수 count와 for반복문에서만 쓰이는 지역변수 count를 혼용한 대표적인 오류!
  if(count >= 7){
    // location.href="exam.php";
    // for(i = 0; i < trueCheck.length; i++){
    //   alert(i + "번째" + trueCheck[i]);
    // }
    $("#member_form").submit();
  } else {
    $.doublecheck();
  }
}
