<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>가입 완료</h1>
    </div>
</section>

<div class="outLayer">
	<div class="result">
		<img src="${ pageContext.request.contextPath }/resources/img/check.ico" class="checkImg">
		<h2>가입이 완료 되었습니다.</h2>
		<hr>
	
		<table>
			<tr>
				<th colspan="2">${ p2pVO.p2pName }</th>
			</tr>
			<tr>
				<td class="td1">연수익률(세전)</td><td class="td2">${ p2pVO.roi }%</td>
			</tr>
			<tr>
				<td class="td1">투자금액</td><td class="td2">${ invAmount }원</td>
			</tr>
		</table>
	
	<input type="button" class="orderCompleteBtn" value="내 투자 현황" onclick="location.href='/mypage'">	
	<input type="button" class="orderCompleteBtn" value="상품 목록" onclick="location.href='/p2p'">
		
	</div>
</div>

<jsp:include page="../include/footer.jsp"/>