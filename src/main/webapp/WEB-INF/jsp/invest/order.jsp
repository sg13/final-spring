<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>투자 금액 확인</h1>
    </div>
</section>

<div class="outLayer">
	<div class="investForm">
		<form method="post" action="${ pageContext.request.contextPath }/invest/order" name="order">
			<div class="subForm">
			<div class="subForm1">
				<h3>투자금액</h3>
				<input id="invAmount" placeholder="투자 금액을 입력해주세요" name="invAmount" class="inputBox" value="${invAmount}"/>
			
				<h3>투자연결 상환계좌</h3>
				<h6>아래 계좌를 투자 및 <br>이자, 상환 계좌로 설정합니다.</h6>
				
				<select name="repayAccount" id="repayAccount">
				    <c:forEach items="${ accountList }" var="list">
				        <c:if test='${ list.type  == "R" }'>
				            <option value="${ list.account }">${ list.bank } ${ list.account }</option>
				        </c:if>
				    </c:forEach>
				</select>
				
				
			</div>
			
			<div class="subForm2">
			
				<h3>간편비밀번호</h3>
				<div class="info_errForm">
					<input type="password" id="simpPwd" name="simpPwd" class="inputBox">
					<input type="hidden" id="hiddenSimpPwd" value="${ simpPwd }">
					<div id="simpPwdErrMsg" class="errMsgStyle">정보를 올바르게 입력해주세요.</div>
				</div>
				
				<h3>투자연결 지불계좌</h3>
				<h6>아래 계좌를 투자 및 <br>이자, 지불 계좌로 설정합니다.</h6>
				<select name="payAccountBank" id="payAccountBank">
				    <c:forEach items="${accountList}" var="list">
				    	<c:if test='${ list.type == "P" }'>
				            <option value="${list.account},${ list.bank }">${list.bank} ${list.account}</option>
				        </c:if>
				    </c:forEach>
				</select>
			</div>
			</div>
			
			<h3>투자 위험 안내</h3>
			<div class="agreementInfo">
				본 상품은 하나은행에서 제공하는 P2P 투자상품이며, 원금이 보장되지 않습니다.<br/>
				모든 투자상품은 ‘유사수신행위의 규제에 관한 법률＇에 의거하여 원금과 수익을 보장할 수 없습니다.<br/>
				차입자가 원금의 전부 또는 일부를 상환하지 못할 경우 발생하는 투자금 손실 등 투자위험은 투자자가 부담하게 됩니다.<br/>
				부실채권 발생시 하나은행은 추심위윔 및 매각 등 원리금상환을 위한 합법적인 추심절차를 진행하며, 이과정에서 투자금의 손실이 발생할 수 있습니다.
			</div>
			
			
			<h4>본인은 투자위험에 대한 내용을 확인했으며, 그 내용에 동의함</h4>
			<div class="info_errForm">
				<input type="text" id="agreeBox" name="agreeBox" class="agreeBox" placeholder="동의">
				<div id="agreementErrMsg" class="errMsgStyle">올바르게 입력해주세요.</div>
			</div>
			<input type="submit" value="가입하기" class="orderBtn">
			<input type="hidden" id="p2pNo" name="p2pNo" value="${ p2pNo }">
			<input type="hidden" id="period" name="period" value="${ period }">
			</form>
		</div>
 </div>
 
<jsp:include page="../include/footer.jsp"/>