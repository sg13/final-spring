<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    


<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>상품 목록 조회</h1>
    </div>
</section>
<div class="outLayer">

<c:if test="${sessionScope.userVO.type == 'S'}">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawChart);


function drawChart(){
	
	$.ajax({
		url:'/p2pChart',
		type: 'get',
		success: function (data) {
	         var chart = JSON.parse(data);
	         var countTop3List = chart.chart1_1;
	         var amountTop3List = chart.chart1_2;
	         var generationChart = chart.chart3;
	         var generationChart2 = chart.chart3;
	         
	         googleChart(countTop3List, amountTop3List, generationChart, generationChart2);
      }
   })
}

function googleChart(countTop3List, amountTop3List, generationChart, generationChart2){
   
    var generationChart_data = new google.visualization.DataTable();

    generationChart_data.addColumn('string', 'GENERATION');
    generationChart_data.addColumn('number', 'GENERATION_RATIO');

    for(var i = 0; i < generationChart.length; i++){
    	generationChart_data.addRows([
          [generationChart[i].GENERATION, generationChart[i].GENERATION_RATIO]
       ])
    };
    
    var generationChart_options = {
     	   'title' : '투자자 연령대 비율',
 	       'fontSize': 20,
 	       'width' :800,
 	       'height' : 500
 	};
    
     var generationChart = new google.visualization.PieChart(document.getElementById('generationRatio'));
     
     generationChart.draw(generationChart_data, generationChart_options);
     
     /* --------세대별 평균 투자액 ------------*/
     var generationChart_data2 = new google.visualization.DataTable();

     generationChart_data2.addColumn('string', 'GENERATION');
     generationChart_data2.addColumn('number', 'GENERATION_AVG_AMOUNT');

     for(var i = 0; i < generationChart2.length; i++){
     	generationChart_data2.addRows([
           [generationChart2[i].GENERATION, generationChart2[i].GENERATION_AVG_AMOUNT]
        ])
     };
     
     var generationChart_options2 = {
      	   'title' : '연령대별 평균 투자액',
  	       'fontSize': 20,
  	       'width' :800,
  	       'height' : 500
  	};
      var generationChart2 = new google.visualization.ColumnChart(document.getElementById('generationAvg'));
      
      generationChart2.draw(generationChart_data2, generationChart_options2);

    /* ------------countTop3List----------- */
    var countTop3List_data = new google.visualization.DataTable();
    
    countTop3List_data.addColumn('number', 'P2PNO');
    countTop3List_data.addColumn('number', 'P2P_COUNT');
    
    for(var i = 0; i < countTop3List.length; i++){
    	countTop3List_data.addRows([
          [countTop3List[i].P2PNO, countTop3List[i].P2P_COUNT]
       ])
    };
    
    var countTop3List_options = {
      	   'title' : '투자 거래량 Top3 상품',
  	       'fontSize': 20,
  	       'width' :800,
  	       'height' : 500
  	};
    
    var countTop3List = new google.visualization.ColumnChart(document.getElementById('countTop3'));
    
    countTop3List.draw(countTop3List_data, countTop3List_options);
    
    /*---------- 투재 거래액 Top3 --------*/
						 
var amountTop3List_data = new google.visualization.DataTable();
    
	amountTop3List_data.addColumn('number', 'P2PNO');
	amountTop3List_data.addColumn('number', 'P2P_AMOUNT');
    
    for(var i = 0; i < amountTop3List.length; i++){
    	amountTop3List_data.addRows([
          [amountTop3List[i].P2PNO, amountTop3List[i].P2P_AMOUNT]
       ])
    };
    
    var amountTop3List_options = {
      	   'title' : '투자 거래액 Top3 상품',
  	       'fontSize': 20,
  	       'width' :800,
  	       'height' : 500
  	};
    
    var amountTop3List = new google.visualization.ColumnChart(document.getElementById('amountTop3'));
    
    amountTop3List.draw(amountTop3List_data, amountTop3List_options);
}
</script>

	<div class="chart">
		<h3>p2p 상품 반응 정보</h3>
		<div class="p2pChart" id="p2pChart" style="width:900px; height: 500px;">
			<div id="countTop3">
			</div>
			<div id="amountTop3">
			</div>
		</div>
		
		<div class="basicChart">
			<h3>투자자 정보</h3>
			<table>
				<tr>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/avg_roi.png" class="chartIcon"/>
					</td>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/total_invAmount.png" class="chartIcon"/>
					</td>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/male.png" class="chartIcon"/>
					</td>
				</tr>
				<tr>
					<td>
						<span class="chart_item_name">평균 금리</span>
					</td>
					<td>
						<span class="chart_item_name">누적 투자액</span>
					</td>
					<td>
						<span class="chart_item_name">총 투자자</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="chart_item_digit">9.45%</span>
					</td>
					<td>
						<span class="chart_item_digit">266억</span>
					</td>
					<td>
						<span class="chart_item_digit">8283명</span>
					</td>
				</tr>
				<tr>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/avg_invAmount.png" class="chartIcon"/>
					</td>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/total_invAmount.png" class="chartIcon"/>
					</td>
					<td>
						<img src="${ pageContext.request.contextPath }/resources/img/coin.png" class="chartIcon"/>
					</td>
				</tr>
				<tr>
					<td>
						<span class="chart_item_name">평균 금리</span>
					</td>
					<td>
						<span class="chart_item_name">누적 투자액</span>
					</td>
					<td>
						<span class="chart_item_name">최다 분산 투자자</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="chart_item_digit">9.45%</span>
					</td>
					<td>
						<span class="chart_item_digit">266억</span>
					</td>
					<td>
						<span class="chart_item_digit" id="coin">828개</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="genearation_gender_chart">
			<h3>성별 투자 정보</h3>
			<div class="genderChart">
				<table>
					<tr>
						<td><span class="chart_item_name">남성</span></td>
						<td></td>
						<td><span class="chart_item_name">여성</span></td>
					</tr>
					<tr>
						<td><img src="${ pageContext.request.contextPath }/resources/img/male.png" class="chartIcon2"/></td>
						<td></td>
						<td><img src="${ pageContext.request.contextPath }/resources/img/female.png" class="chartIcon2"/></td>
					</tr>
					<tr>
						<td><span class="chart_item_digit">345만원</span></td>
						<td><span class="chart_item_name2">평균 투자액</span></td>
						<td><span class="chart_item_digit">345만원</span></td>
					</tr>
					<tr>
						<td><span class="chart_item_digit">67.5%</span></td>
						<td><span class="chart_item_name2">비율</span></td>
						<td><span class="chart_item_digit">32.5%</span></td>
					</tr>
					<tr>
						<td><span class="chart_item_digit">34.4세</span></td>
						<td><span class="chart_item_name2">평균 연령</span></td>
						<td><span class="chart_item_digit">34.1세</span></td>
					</tr>
				</table>
			</div>
		
			<h3>연령별 평균 투자 금액 및 투자자 비율</h3>
			<div class="generationChart" id="generationChart">
				<div id="generationRatio">
				</div>
				<div id="generationAvg">
				</div>
				
			</div>
		</div>
	</c:if>
	
	</div>

	<div id="general_merchandise">
	    <div class="table" data-toggle="table">
	        <div class="thead">
	            <div class="tr">
	                <div class="th">상점</div>
	                <div class="th"><div class="verticalLine"></div>연수익률</div>
	                <div class="th"><div class="verticalLine"></div>상환기간</div>
	                <div class="th"><div class="verticalLine"></div>모집현황</div>
	                <div class="th"><div class="verticalLine"></div>모집률</div>
	                <div class="th"><div class="verticalLine"></div>버튼</div>
	            </div>
	        </div>
	        <div class="tbody">
	        <c:choose>
		        <c:when test="${ empty userVO }">
	        		<input type="hidden" id="custId_invest" value="">
				</c:when>
				<c:otherwise>
	        		<input type="hidden" id="custId_invest" value="${ userVO }">
				</c:otherwise>
			</c:choose>	      		
	      		<c:forEach items="${ p2pList }" var="p2p" varStatus="status">
	        		<div class="tr">
	                	<div class="tbody-head">
<!-- 							상품명 -->
							<div class="td">
								<a style="pointer-events: none; cursor: default;" class="merchandise_name">
									<span class="merchandise-idx">
										<c:out value="${ p2p.p2pNo }"/>
										<input type="hidden" id="merchandise-idx${p2p.p2pNo}" name="merchandise-idx" value="${ p2p.p2pNo }"/>
									</span>
									<span class="merchandise-title">
										<c:out value="${ p2p.p2pName }"/>
										<input type="hidden" id="merchandise-title" name="merchandise-title" value="${ p2p.p2pName }"/>
									</span>
		                        </a>
		                    </div>
		                </div>				
		                <div class="tbody-body">
		                    <dl>
<!-- 		                    	연수익률 -->
		                        <div class="td">
									<dd><c:out value="${ p2p.roi }"/>%</dd>
									<input type="hidden" id="interest_rate" name="interest_rate" value="${ p2p.roi }"/>
								</div>
								
<!-- 								상환기간 -->
		                        <div class="td">
		                            <dt>상환기간</dt>
									<dd><c:out value="${ p2p.period }"/>개월</dd>
									<input type="hidden" id="loan_period${p2p.p2pNo}" name="loan_period" value="${ p2p.period }">
		                        </div>
		                        
<!-- 		                        모집금액 -->
		                        <div class="td">
		                            <dd>
				                        <c:out value="${p2p.currentAmount }"/><span class="slash">&#47;</span> <c:out value="${ p2p.totalGoal }"/> 만원</small>
				                        <input type="hidden" id="totalGoal" name="totalGoal" value="${ p2p.totalGoal }">
				                     </dd>
				                </div>
				                
<!-- 				                모집률 -->
		                        <div class="td" style="width: 30px; margin: 0 100px;">
		                        	<dt>모집률</dt>
				                    <c:choose>
		                        		<c:when test="${ p2p.ratio gt 50}"  >
						                    <div class="progress-circle over50 p${ p2p.ratio }">
		                        		</c:when>
		                        		<c:otherwise>
						                    <div class="progress-circle p${ p2p.ratio }">
		                        		</c:otherwise>
		                        	</c:choose>
							   			<span class="graphDigit">${ p2p.ratio }%</span>
							   			<div class="left-half-clipper">
									      	<div class="first50-bar"></div>
									      	<div class="value-bar"></div>
								   		</div>
									</div>
								</div>
				                
<!-- 				                버튼 -->
								 <div class="td">
									<c:choose>
										<c:when test="${ sessionScope.userVO.type == 'S' }">
											<button type="button" id="p2pDeleteBtn" class="adminBtn" onclick="deleteP2PBtn()">삭제하기</button>
										</c:when>
										<c:otherwise>
											<button type="button" class="invest-btn" data-toggle="modal" data-target="#invest-btn${status.index}">투자하기</button>
											<button type="button" class="detail-btn">상세보기</button>											
										</c:otherwise>
									</c:choose>	
									
<!-- 									Modal: modalCart -->
									<input type="hidden" id="hidden_index" value="${status.index}"/>
									<div class="modal fade" id="invest-btn${status.index}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									      
<!-- 									     	Body -->
										     <div class="box_style_1" id="expose">
												<h3 class="inner">
												투자금액 설정
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">×</span>
										        </button>
												</h3>
												
												<div class="input-amount">
													<div class="input-group">
														<input class="text-right form-control input" type="text" id="amount"
											                   class='amount_invest' name="amount" value=""
											                   placeholder="1만원 단위로 입력하세요" numberOnly/>
											            <span class="input-group-addon">만원</span>
											        </div>
											    </div>
											    <div class="add-amount">
											        <input class="col-xs-3 btn" type="button" id="100" value="100만"  checked>
											        <input class="col-xs-3 btn" type="button" id="10" value="10만"  checked>
											        <input class="col-xs-3 btn" type="button" id="1" value="1만"  checked>
											        <input class="col-xs-3 btn" type="button" id="0" value="정정"  checked>
											    </div>
											
											    <div class="money-range">
											        <p style="text-align: left;">
											        	최대 투자 한도
											        	<span>
											        		<c:out value="${ p2p.totalGoal }"/>만원
											        	</span>
											        	<input type="hidden" id="totalGoal" value="${ p2p.totalGoal }"/>
											        </p>
											    </div>
											    <div class="my-money">
											         <p style="text-align: left;">
											         	연 수익률
											         	<span>
											         		<c:out value="${ p2p.roi }"/>%
											         	</span>
											         	<input type="hidden" id="roi" value="${ p2p.roi }"/>
											         </p>
											    </div>
											    
												<button class="btn_full" type="button" data-id="${p2p.p2pNo}" id="투자하기">투자하기</button>
													<div class="text-center">
	<!-- 													<button type="button" id="상환일정보기" class="invest-btn" data-toggle="modal" data-target="#schedule">상환 일정 보기</button> -->
														<button type="button" id="상환일정보기" class="invest-btn">상환 일정 보기</button>
												    </div>
													<div id="orgSchedule">	    
														<table class="table table-hover" id="scheduleTable">
														          <thead>
														          <tr>
														              <th>회차</th>
														              <th>원금</th>
														              <th>세전이자</th>
														              <th>세금</th>
														              <th>세후합계</th>
														          </tr>
														          </thead>
														          <tbody>
														          </tbody>
														  </table>
													  </div>
											</div>
									      </div>
									    </div>
									  </div>
									  
									  
								
<!-- 									Modal: modalCart: "상환일정" -->
<!-- 									<div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<!-- 									  <div class="modal-dialog" role="document"> -->
<!-- 									    <div class="modal-content"> -->
<!-- 									      Header -->
<!-- 									      <div class="modal-header"> -->
<!-- 									        <h4 class="modal-title" id="myModalLabel">상환일정</h4> -->
<!-- 									        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
<!-- 									          <span aria-hidden="true">×</span> -->
<!-- 									        </button> -->
<!-- 									      </div> -->
<!-- 									      Body -->
<!-- 									      <div class="modal-body"> -->
<!-- 									        <table class="table table-hover" id="scheduleTable"> -->
<!-- 									          <thead> -->
<!-- 									          <tr> -->
<!-- 									              <th>회차</th> -->
<!-- 									              <th>원금</th> -->
<!-- 									              <th>세전이자</th> -->
<!-- 									              <th>세금</th> -->
<!-- 									              <th>세후합계</th> -->
<!-- 									          </tr> -->
<!-- 									          </thead> -->
<!-- 									          <tbody> -->
<!-- 									          </tbody> -->
<!-- 									        </table> -->
<!-- 									      </div> -->
<!-- 									      Footer -->
<!-- 									      <div class="modal-footer"> -->
<!-- 									        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">확인</button> -->
<!-- 									      </div> -->
<!-- 									    </div> -->
<!-- 									  </div> -->
<!-- 									</div> -->
								</div><!-- td -->
	                    	</dl>
		                </div><!-- tbody-body -->
					</div><!-- tr -->
				</c:forEach>
				<c:if test="${sessionScope.userVO.type == 'S'}">
				<form:form method="post" commandName="p2pVO" action="${ pageContext.request.contextPath }/p2pNew">
				<div class="newP2Pform">
					<table>
						<tr>
							<th colspan="2">상점</th>
	                		<th>연수익률</th>
	                		<th>상환기간</th>
	                		<th>모집총액</th>
	                		<th></th>
						</tr>
						<tr>
							<td>
								<form:input path="p2pNo" id="newP2PNo" name="p2pNo" value=""/>
							</td>
							<td>
								<form:input path="p2pName" id="newP2PName" name="p2pName" value=""/>
							</td>
							<td>
								<form:input path="roi" id="newROI" name="roi" value=""/>
							</td>
							<td>
								<form:input path="period" id="newPeriod" name="period" value=""/>
							</td>
							<td>
								<form:input path="totalGoal" id="newTotalGoal" name="totalGoal" value=""/>
							</td>
							<td>
								<button type="submit" class="adminBtn">추가하기</button>
							</td>
						</tr>
					</table>
					</div>
				</form:form>
				</c:if>
			</div><!-- tbody -->
	     </div><!-- table -->
	  </div>
	</div>

<jsp:include page="../include/footer.jsp"/>