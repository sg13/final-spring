<jsp:include page="../include/header.jsp"/>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>커뮤니티 게시글 수정</h1>
    </div>
</section>
						    
							<form:form id="uploadForm" method="post" commandName="replyboardVO" action="${ pageContext.request.contextPath }/replyBoard/inquiry_update">
								<form:hidden path="writer" value="${ userVO.name }"/>
								<form:hidden path="no" value="${ boardVO.no }"/>
								<table style="margin:auto">
									<thead>
										<tr><th width="10%">제목</th>
											<td><form:input path="title" value="${ boardVo.title }"/><form:errors path="title" class="red"/></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th width="10%" style="vertical-align:top">내용</th>
											<td>
												<form:textarea path="content" id="content" style="height: 500px" value="${ boardVO.content }"/>
												<form:errors path="content" class="red"/>
											</td>
										</tr>
								   </tbody>
								</table><br>
								<input type="button" class="smallbtn" onclick="location.href='${ pageContext.request.contextPath}/replyBoard'" value="취소" />
								<input type="button" class="smallbtn"
													 onClick="updateContent()"
								                     value="등록">
								</form:form> 

<jsp:include page="../include/header.jsp"/>