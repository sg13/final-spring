<jsp:include page="../include/header.jsp"/>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>커뮤니티 게시글 조회</h1>
    </div>
</section>
				<div class="container margin_60">
			<div class="row">
				<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
					
				</aside><!-- End aside -->
				
				<div class="col-lg-9 col-md-8 tab-content">
					<div class="review_strip notice">
						<div class="row margin-bottom-15 text-center">
							<div class="col-xs-12 text-center">
								<span class="h3">커뮤니티</span>
							</div>            	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-list table-hover table-news">
									<colgroup>
										<col width ="10%"/>
										<col width ="80%"/>
										<col width =10%"/>
									</colgroup>
										<thead>
											<tr>
											<th>작성자</th>
											<th>제목</th>
										 	<th>조회수</th>
											</tr>
										</thead>
								
										<tbody>
										
										<c:forEach items="${ replyboardList }" var="replyboard">
											<tr>
												<td>${ replyboard.writer }</td>
												<td style="text-align:left">
												<a href="${ pageContext.request.contextPath }/replyBoard/${ replyboard.no }">
													<c:if test="${ replyboard.relevel eq '1' }">
													└>
													</c:if>
													<c:out value="${ replyboard.title }"/>
												</a></td>
												<td>${ replyboard.viewCnt }</td>
											</tr>
										</c:forEach>
									</tbody>
								</table><br><br>
							</div>
							
							
							
							
					<c:if test="${ userVO.type eq 'U' }">
						<div align='right'>
						<input type="button" class="smallbtn" value="작성" onClick="location.href='${ pageContext.request.contextPath }/replyBoard/inquiry_write?type=post&rootid=0'"/>
						</div>
					 </c:if>
 					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		 

		<script>
			$(document).on('click', '#btnSearch', function(e){
			      e.preventDefault();

			      var url = "${pageContext.request.contextPath}/replyBoard";
			      url = url + "?searchType=" + $('#searchType').val();
			      url = url + "&keyword=" + $('#keyword').val();
			      location.href = url;
			      console.log(url);
			   });   
			   
			   function fn_prev(page, range, rangeSize) {

//			          var page = ((range - 2) * rangeSize) + 1;
			         var page = page - 1;
//			          var range = range - 1;
			         var url = "${pageContext.request.contextPath}/replyBoard";
			         url = url + "?page=" + page;
			         url = url + "&range=" + range;
			         location.href = url;

			      }
			      //페이지 번호 클릭

			      function fn_pagination(page, range, rangeSize, searchType, keyword) {

			         var url = "${pageContext.request.contextPath}/replyBoard";
			         url = url + "?page=" + page;
			         url = url + "&range=" + range;
			         location.href = url;   

			      }
			      //다음 버튼 이벤트

			      function fn_next(page, range, rangeSize) {

			         var page = parseInt(page) + 1;
//			          var range = parseInt(range) + 1;
			         var url = "${pageContext.request.contextPath}/replyBoard";
			         url = url + "?page=" + page;
			         url = url + "&range=" + range;
			         location.href = url;
			      }
			      </script>

<jsp:include page="../include/header.jsp"/>