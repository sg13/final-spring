<jsp:include page="../include/header.jsp"/>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 


<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>글 조회</h1>
    </div>
</section>


<div class="outLayer">
    <div class="container margin_60">
			<div class="row">
				<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
				</aside><!-- End aside -->

				<div class="col-lg-9 col-md-8 tab-content">
					<div class="review_strip notice">
						<div class="row margin-bottom-15 text-center">
							<div class="col-xs-12 text-center">
								<span class="h3">커뮤니티</span>
							</div>            	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive" id="noticeboard">
								<table class="table table-list table-hover table-news">
									<input type="hidden" name="rootid" value="${ replyboard.rootid }"/>
										<tr>
											<th>번호</th><td>${ replyboard.no }</td>
										</tr>
										<tr>
											<th>작성자</th><td>${ replyboard.writer }</td>
										</tr>
										<tr>
											<th>제목</th><td>${ replyboard.title }</td>
										</tr>
										<tr>
											<th>내용</th>
											<td>${ replyboard.content }</td>
										</tr>
									</table>
									
									<div class="customP2PtableDiv">
										<table id="customP2Ptable">
									          <thead>
									          <tr>
									              <th>상품번호</th>
									              <th>투자 금액</th>
									              <th>거래일</th>
									              <th>만기일</th>
									          </tr>
									          
									          </thead>
									          <tbody>
									          </tbody>
										</table>
									</div>
									
									
									<input type="hidden" id="custIdP2P" value="${ replyboard.writer }"/>
									
									</div>
									<br>
									<div align='right'>
									<c:if test="${ replyboard.relevel eq '0' }">
											<input type="button" class="smallbtn" onclick="doAction('C')" value="답글 작성" />
									</c:if>
									<c:if test="${ userVO.name eq replyboard.writer }">
										<input type="button" class="smallbtn" onclick="doAction('U')" value="수정" />
										<input type="button" class="smallbtn" onclick="doAction('D')" value="삭제" />
									</c:if>
										<input type="button" class="smallbtn" onclick="doAction('L')" value="목록으로" />
										<button type="button" class="detail-btn" id="customP2P">해당 고객 포트폴리오보기</button>	
									</div>

						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		
					


	<script>
	
	function doAction(type) {
		switch(type) {
		case 'C' : 
			location.href = "${ pageContext.request.contextPath}/replyBoard/inquiry_write?type=reply&rootid=${ replyboard.no }";
			break;
		case 'U' : 
			location.href = "${ pageContext.request.contextPath}/replyBoard/inquiry_update/${replyboard.no}";
			break;
		case 'D' : 
			if(confirm('${replyboard.no}번을 삭제하시겠습니까?')) {
				// 해당 게시물 삭제후 목록페이지로 이동
				
				$.ajax({
					url: '${ pageContext.request.contextPath }/replyBoard/${replyboard.no}',
					type: "delete",
					success : function(data) {
						alert(JSON.parse(data).msg);
						location.href = "${ pageContext.request.contextPath}/replyBoard";
					}
				});
				
			
			} 
			break;
		case 'L' : 
			location.href = "${ pageContext.request.contextPath}/replyBoard";
			break;
		}
	}
</script>

<jsp:include page="../include/footer.jsp"/>