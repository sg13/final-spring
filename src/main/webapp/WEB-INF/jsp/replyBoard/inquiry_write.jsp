<jsp:include page="../include/header.jsp"/>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  
<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>커뮤니티 게시글 작성</h1>
    </div>
</section>

<div class="container margin_60">
			<div class="row">
			<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
					
				</aside><!-- End aside -->
			
				<div class="col-md-8 col-sm-8">
					<div class="step">
							<c:if test="${ type eq 'post' }">
							<div class="row margin-bottom-15 text-center">
								<div class="col-xs-12 text-center">
									<span class="h3">새글 작성</span>
								</div>            	
							</div>
							</c:if>
							<c:if test="${ type eq 'reply' }">
							<div class="row margin-bottom-15 text-center">
								<div class="col-xs-12 text-center">
									<span class="h3">답글 작성</span>
								</div>            	
							</div>
							</c:if>
						    <div class="step">
							<form:form id="uploadForm" method="post" commandName="replyboardVO" action="${ pageContext.request.contextPath }/replyBoard/inquiry_write">
							
								<c:if test="${ type eq 'reply' }">
								<form:hidden path="rootid" value="${ rootid }"/>
								<form:hidden path="relevel" value="1"/>
								</c:if>
								<c:if test="${ type eq 'post' }">
								<form:hidden path="rootid" value="${ rootid }"/>
								<form:hidden path="relevel" value="0"/>
								</c:if>
								<div class="row">
								<div class="col-sm-10">
									<div class="form-group">
										<label>제목</label>
										<form:input path="title"/><form:errors path="title" class="red"/>
									</div>
								</div>
								<div class="col-sm-10">
									<div class="form-group">
										<label>작성자</label>
										<c:out value="${ userVO.id }" />
										<form:hidden path="writer" value="${ userVO.id }"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<div class="form-group">
										<label>문의내용</label>
											<form:textarea path="content" id="content" name="content" class="form-control" placeholder="문의내용을 입력해주세요" style="height:200px;"/>
										<form:errors path="content" class="red"/>
									</div>
								</div>
							</div>
								<input type="button" class="smallbtn" onclick="location.href='${ pageContext.request.contextPath}/replyBoard'" value="취소" />
								<input type="submit" class="smallbtn" value="등록">
								</form:form> 
						</div>
					</div>
				</div>
			</div>
		</div>
<jsp:include page="../include/footer.jsp"/>