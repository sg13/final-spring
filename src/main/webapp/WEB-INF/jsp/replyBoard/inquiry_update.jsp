<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>공지 게시판 수정</h1>
    </div>
</section>

		<div class="container margin_60">
			<div class="row">
			<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
					
				</aside><!-- End aside -->
			
				<div class="col-md-8 col-sm-8">
					<div class="form_title">
						<h3><strong><i class="icon-pencil"></i></strong>공지글 작성</h3>
						<p></p>
					</div>
					
					<div class="step">

							<form:form id="uploadForm" method="post" commandName="replyboardVO" action="${ pageContext.request.contextPath }/replyBoard/inquiry_update">
								<form:hidden path="no" value="${ replyboardVO.no }"/>
								<form:hidden path="writer" value="${ userVO.name }"/>
								<table style="margin:auto">
									<thead>
										<tr><th width="10%">제목</th>
											<td><form:input path="title" value="${ replyboardVO.title }"/><form:errors path="title" class="red"/></td>
										</tr>
									</thead>
									<thead>
									<tbody>
										<tr>
											<th width="10%" style="vertical-align:top">내용</th>
											<td>
												<form:textarea path="content" id="content" style="height: 500px" value="${ replyboardVO.content }"/>
												<form:errors path="content" class="red"/>
											</td>
										</tr>
								   </tbody>
								</table><br>
								<input type="file" id="noticeFile" name="noticeFile"/><br><br>
								<input type="button" class="smallbtn" onclick="${ pageContext.request.contextPath}/replyBoard" value="취소" />
								<input type="button" class="smallbtn"
													 onClick="updateContent()"
								                     value="등록">
								</form:form> 
								
      						</div>
   						</div>
					</div>
			</div>
