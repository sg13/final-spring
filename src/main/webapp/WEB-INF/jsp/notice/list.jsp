<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
	$(document).ready(function() {
		$('#writeBtn2').click(function() {
			location.href = "${ pageContext.request.contextPath }/notice/write";
		});
	});
	
/* 	function doAction(noticeNo) {
		<c:if test="${ empty userVO }">
			if(confirm('로그인 후 가능한 서비스입니다\n로그인 하시겠습니까?')) {
				location.href = "${ pageContext.request.contextPath }/login";
			}
		</c:if>
		<c:if test="${ not empty userVO }">
			location.href = "${ pageContext.request.contextPath }/notice/" + noticeNo;
		</c:if>
	} */
</script>


<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>공지 게시판</h1>
    </div>
</section>

		<div class="container margin_60">
			<div class="row">
				<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
					
				</aside><!-- End aside -->
				
				<div class="col-lg-9 col-md-8 tab-content">
					<div class="review_strip notice">
						<div class="row margin-bottom-15 text-center">
							<div class="col-xs-12 text-center">
								<span class="h3">공지사항</span>
							</div>            	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-list table-hover table-news">
										<thead>
											<tr>
												<th>제목</th>
												<th>작성일</th>
											</tr>	
										</thead>
										<tbody>
											<c:forEach items="${ noticeList }" var="notice">
												<tr>
													<td class="single-line">
														<a href="${ pageContext.request.contextPath }/notice/${ notice.no }">
															<c:out value="${ notice.title }"/>
														</a>
													</td>
													<td>${ notice.regDate }</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								<div align="right">
								<%-- <c:if test="${ not empty userVO }"> --%>
									<button id="writeBtn2">새글 등록</button>
								<%-- </c:if> --%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


<jsp:include page="../include/footer.jsp"/>