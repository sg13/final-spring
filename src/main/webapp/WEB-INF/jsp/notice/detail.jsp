<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script> -->


<script>
	function doAction(type) {
		switch(type) {
		case 'U' : 
			location.href = "${ pageContext.request.contextPath }/notice/modify/${notice.no}";
			break;
		case 'D' : 
			if(confirm('${notice.no}번을 삭제하시겠습니까?')) {
				// 해당 게시물 삭제후 목록페이지로 이동
				
				/* ajax로 받아지는 모든 정보들을 통상적으로 data라고 함(추정) */
				$.ajax({
					/* delete메소드에서 받은 uri */
					url: '${ pageContext.request.contextPath }/notice/${notice.no}',
					/* deleteMapping이었으니... */
					type: "delete",
					/* delete할 때 map으로 던진 놈. data 종류는 json */
					/* 요사이에 실행되는 것이 @DeleteMapping으로 매핑된 delete메소드 */
					/* 성공되면! 다음 function 실행*/
					success : function(data) {
						/* json 내용인데 string으로 받아짐. 문자열이면 key랑 value 구분을 못하니
						   data로 형변환해서 key값이 msg인 애를 alert해라! */
						alert(JSON.parse(data).msg);
						
						/* alert에서 확인 누르면 notice목록창으로 가라고 */
						location.href = "${ pageContext.request.contextPath}/notice";
					}
				});
			} 
			break;
		case 'L' : 
			location.href = "${ pageContext.request.contextPath}/notice";
			break;
		}
	}
</script>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>게시물 상세 페이지</h1>
    </div>
</section>
		<div class="container margin_60">
			<div class="row">
				<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
				</aside><!-- End aside -->

				<div class="col-lg-9 col-md-8 tab-content">
					<div class="review_strip notice">
						<div class="row margin-bottom-15 text-center">
							<div class="col-xs-12 text-center">
								<span class="h3">공지사항</span>
							</div>            	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive" id="noticenotice">
									<table class="table table-list table-hover table-news">
										<tr>
											<th>번호</th>
											<td>${ notice.no }</td>
										</tr>
										<tr>
											<th>제목</th>
											<td>${ notice.title }</td>
										</tr>
										<tr>
											<th>글쓴이</th>
											<td>${ notice.writer }</td>
										</tr>
										<tr>
											<th>내용</th>
											<td>${ notice.content }</td>
										</tr>
										<tr>
											<th>조회수</th>
											<td>${ notice.viewCnt }</td>
										</tr>
										<tr>
											<th>등록일</th>
											<td>${ notice.regDate }</td>
										</tr>
									</table>
								</div>
									<br>
									<div align="right">
									<button onclick="doAction('U')">수정</button>&nbsp;&nbsp;
									<button onclick="doAction('D')">삭제</button>&nbsp;&nbsp;
									<button onclick="doAction('L')">목록</button>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
<jsp:include page="../include/footer.jsp"/>
