<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<section id="hero" class="login-header" style="background: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 tablet-none affix">
                <div class="register">
                    <p>투자를 통해 상생의 가치를 실현하는 ‘관계 금융’의 내일,<br>
                    	지금, 하나은행과 함께 시작하세요!</p>

                    <h3 class="text-blue">투자의 달인을 뒤이은 함께 투자 솔루션</h3>
                    <p>하나은행은 5 ~ 15% 수준의 중금리 신용 대출 서비스를 제공합니다.</p>

                    <h3 class="text-blue">초저금리 시대, 새로운 재테크의 시작</h3>
                    <p style="margin-bottom:0px;">투자자들은 하나은행이 엄선한 상점들의 대출채권 투자를 통해 <br> 연 10% 초반의 수익을 기대할 수 있습니다.(세전 기준)</p><br>
                    
                    <p style="margin-bottom:0px;">하나은행은 동산 및 부동산의 담보 등의 장치를 통해 채권의 안정성을 강화해나가는 한편,</p><br>
                    <p class="last">다양한 리워드와 추가 금리 혜택 등으로 투자의 즐거움을 제공합니다.</p>
                    <p style="font-size:11px; color: #ccc">* 리워드와 추가 금리 적용은 각 채권상품에 따라 상이하게 적용됩니다.</p>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-8">
                <form:form method="post" commandName="memberVO" action="${ pageContext.request.contextPath }/member/join">
                    <div class="login join" id="register-box" style="margin: 90px 0px 100px;">
                        <h3>회원가입</h3>
                        <hr>

                        <div class="form-group input-group">
                            <div><img src="${ pageContext.request.contextPath }/resources/img/id.png" class="joinIcon"/>아이디</div>
                            <form:input path="id" type="text" class="form-control input" name="id" id="id" placeholder="아이디를 입력해주세요(15자이내)"/>
                            <input type="button" id="duplicateIdCheck" class="ddd" value="중복 인증"/>
                        </div>
                        <div class="form-group input-group">
                            <div><img src="${ pageContext.request.contextPath }/resources/img/id.png" class="joinIcon"/>이름</div>
                            <form:input path="name" type="text" class="form-control input" name="name" id="name" placeholder="이름을 입력해주세요."/>
                        </div>
                        <div class="form-group input-group" style="margin-bottom: 0px;">
                            <div><img src="${ pageContext.request.contextPath }/resources/img/pw.png" class="joinIcon"/>비밀번호</div>
                            <form:input path="password" type="password" class="form-control input" name="pwd1" id="pwd1" placeholder="비밀번호를 입력해주세요(영/한 혼합)"/>
                        </div>

                        <div class="form-group input-group">
                            <div><img src="${ pageContext.request.contextPath }/resources/img/pw.png" class="joinIcon"/>간편 비밀번호</div>
                            <form:input path="simp_pwd" type="password" class="form-control input" name="simp_pwd" id="simp_pwd" placeholder="간편 비밀번호를 입력해주세요(6자)"/>
                        </div>
                        <div class="form-group input-group">
                        	<div><img src="${ pageContext.request.contextPath }/resources/img/pw.png" class="joinIcon"/>휴대전화</div>
                        	<div class="phone">
	                            <form:input path="phone" type="number" class="form-control input" name="phone" id="phone" placeholder="휴대폰번호를 입력해주세요('-' 제외)"/>
	                            <span class="input-group-btn" style="vertical-align: top;">
	                            	<input type="button" class="btn_1 btn-certi" id="verifyBtn" value="인증"/>
	                        	</span>
	                        </div>
                        </div>
                        <div class="form-group input-group display-none">
                            <div class="info_errForm">
	                            <input type="number" class="form-control" id='certi_num' name='certi_num' placeholder="인증번호를 입력해주세요" disabled="disabled"/>
								<div id="pwdErrMsg" class="errMsgStyle">인증번호가 틀렸습니다.</div>
							</div>
                        </div>
                        <div class="form-group input-group">
                            <div><img src="${ pageContext.request.contextPath }/resources/img/email.png" class="joinIcon"/>이메일</div>
                            <form:input path="email" type="text" class="form-control input" name="email" id="email" placeholder="이메일을 입력해주세요"/>
                        </div>
                        
                        <div class="form-group input-group">
                            <div>만 나이 입력</div>
                        	<form:input path="age" type="number" class="form-control input" name="age" id="age" value=""/>
                        </div>
                        
                        <div class="form-group input-group">
                            <div>성별 선택</div>
                            <form:radiobutton path="gender" name="userGender" id="userMale" value="M"/>
                            <label for="userMale">남</label>
                            <form:radiobutton path="gender" name="userGender" id="userFemale" value="F"/>
                            <label for="userFemale">여</label>
                        </div>
                        
                        <button type="submit" class="btn_full" style="margin-bottom: 10px; margin-top:15px;" id="회원가입">가입하기</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</section>

<jsp:include page="../include/footer.jsp"/>