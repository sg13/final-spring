<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>가입 완료</h1>
    </div>
</section>

<div class="outLayer">
	<div class="result">
		<img src="${ pageContext.request.contextPath }/resources/img/check.ico" class="checkImg">
		<h2>${ id }고객님, 투달딸에 가입해주셔서 감사합니다.</h2>
		<hr>
	
	<input type="button" class="orderCompleteBtn" value="로그인 하러 가기" onclick="location.href='/login'">	
	<input type="button" class="orderCompleteBtn" value="상품 목록 살펴보기" onclick="location.href='/p2p'">
		
	</div>
</div>

<jsp:include page="../include/footer.jsp"/>