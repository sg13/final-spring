<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
function joinOut(){
	
	var custId = $('#custId').val();
	console.log(custId);
    var pwd = $('#user_password').val();
	console.log(pwd);
	
	$.ajax({
		url: '/member/pwdCheck/'+custId + '/' + pwd,
		/* deleteMapping이었으니... */
		type: "delete",
		/* delete할 때 map으로 던진 놈. data 종류는 json */
		/* 요사이에 실행되는 것이 @DeleteMapping으로 매핑된 delete메소드 */
		/* 성공되면! 다음 function 실행*/
		success : function(data) {
			console.log(data);
			alert(data);
			/* json 내용인데 string으로 받아짐. 문자열이면 key랑 value 구분을 못하니
			   data로 형변환해서 key값이 msg인 애를 alert해라! */
// 			alert(JSON.parse(data).msg);
			
			var id = JSON.parse(data).custId;
			
			/* alert에서 확인 누르면 board목록창으로 가라고 */
			location.href = "/member/joinoutComplete/" + id;
		}
	});
}

</script>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>비밀번호 재확인</h1>
    </div>
</section>

<div class="outLayer">
	<section id="hero" class="login-header">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="login">
                   		<div class="text-center">
                               <h3 style="margin-top: 0px; margin-bottom: 20px;">비밀번호 입력</h3>
                           </div>
							<div class="form-group input-group">
								<span class="input-group-addon"><img src="${ pageContext.request.contextPath }/resources/img/pw.png" class="loginIcon"/></span>
                                <input path="pwd" type="password" class="form-control" id="user_password" name="pwd" placeholder="비밀번호를 입력해주세요" value=""/>
                                <input type="hidden" id="custId" value="${ userVO.id }"/>
							</div>
						<input type="button" class="btn_full" id="회원탈퇴" value="확인" onclick="joinOut()"/>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<jsp:include page="../include/footer.jsp"/>
