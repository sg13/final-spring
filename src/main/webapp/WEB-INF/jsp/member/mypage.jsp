<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>마이 투달딸</h1>
    </div>
</section>

<div class="outLayer">
	 <div class="container margin_60">
		<div class="row">
			<aside class="col-lg-3 col-md-4">
				<div class="review_strip">
					<h4>
			            <span class="text-blue single-line">김나라</span>님 <a href="../myfunda/login"><i class="icon_set_1_icon-65 pull-right font-15"></i></a>
			            <br>
			            <small><a href="http://blog.naver.com/fundamaster/221013329373" target="_blank">(개인투자자)</a></small>
			        </h4>
					
					
			        <strong>총 0개 채권에 투자하셨습니다</strong>
					<ul id="profile_summary" class="profile_summary_with_border">
			            <li>총 입금액<span>0 원</span></li>
			            <li>총 출금액<span>0 원</span></li>
			            <li>잔여 총 자산<span>0 원</span></li>
			            <div style="padding: 5px 10px 5px 25px;">
			                <div class="row">
			                    <small>투자중인 원금</small> <span style="float: right;">0 원</span>
			                </div>
			            </div>
			            <li>투자수익률<span>연 0 %(세전)</span></li>
			        </ul>
					
					<!-- Button trigger modal-->
					<button type="button" data-href="/v2/invest/?page=5517" class="detail-btn" data-toggle="modal" data-target="#accountAdd_modal" id="detail-btn">계좌 등록</button>
					<a href="/member/pwdCheck">회원 탈퇴</a>
					
					<!-- Modal: modalCart -->
					<div class="modal fade" id="accountAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
					  aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <!--Header-->
					      <div class="modal-header">
					        <h4 class="modal-title" id="myModalLabel">계좌 등록</h4>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_close">
					          <span aria-hidden="true">×</span>
					        </button>
					      </div>
					      <!--Body-->
					      <div class="modal-body">
					
					        <div class="form-group input-group">
                            <select class="form-control" name="referral" id="modal_bank">
                                <option value="">은행/증권사 선택</option>
                                <option value="하나">하나은행</option>
                                <option value="신한">신한은행</option>
                                <option value="농협">농협은행</option>
                                <option value="국민">국민은행</option>
                            </select>
                            <div class="accountType" id="acountType" name="accountType">
	                            <div>계좌 용도 선택</div>
		                            <input type="radio" name="payType" id="repay" value="R"/>
		                            <label for="repay">상환용</label>
		                            <input type="radio" name="payType" id="pay" value="P"/>
		                            <label for="pay">지불용</label>
	                            </div>
	                        </div>
	                        
	                        <div class="form-group input-group">
	                            <input type="text" class=" form-control input" name="phone" id='modal_account' placeholder="계좌번호를 입력해주세요('-' 제외)" numberOnly>
	                            <span class="input-group-btn" style="vertical-align: top;">
	                            <button class="btn_1 btn-certi" id="account_veri_btn">인증</button>
	                        	</span>
	                        </div>
							<div class="form-group input-group" id="accountCheck">
	                            <div class="accountMsgChk1">
	                            	<strong>1원</strong>을 보냈습니다.
		                            <div>본인의 계좌인지 확인하기 위해, 입력하신 계좌로<br> 하나은행이 1원을 입금했습니다.</div>
		                        </div>
	                            <div class="accountMsgChk2">
		                            <div>1.  계좌의 거래내역에서 입금된 1원의 입금자명을 확인하고 오세요.</div>
		                            <div>2.  확인한 입금자명을 입력해주세요.</div>
		                        </div>
	                            <input type="text" class="form-control input" name="email" id="modal_depositor" placeholder="입금자명 네 글자를 입력해주세요.">
	                            <input type="hidden" value="" id="hidden_depositor"/>
	                            <div id="depositorErrMsg" class="errMsgStyle">입금자명을 올바르게 입력해주세요.</div>
					      	</div>
					      <!--Footer-->
					      <div class="modal-footer">
					        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" id="accountAdd">등록</button>
					      </div>
					      </div>
					    </div>
					  </div>
					</div>
					<!-- Modal: modalCart -->
					
					
				</div>
				<div class="box_style_cat">
					<ul id="cat_nav">
						<li><a href="../myfunda/dashboard" id="active"></i>투자현황</a></li>
						<li><a href="../myfunda/list" id=""></i>투자내역</a></li>
						<li><a href="../myfunda/schedule/list" id=""></i>상환스케쥴</a></li>
						<li><a href="../myfunda/deposit" id=""></i>예치금 충전/출금</a></li>
						<li><a href="../myfunda/transfer" id=""></i>입출금내역</a></li>
			            <li><a href="../myfunda/autoinvest" id=""></i>자동 분산투자</a></li>
			            <li><a href="../myfunda/point" id=""></i>포인트</a></li>
					</ul>
				</div>
			</aside>

                <div class="col-lg-9 col-md-8">
                    <div class="review_strip">
                        <div class="row">
                            <h3 class="h4-title">투자정보</h3>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="status_summary" class="table table_summary">
                                        <thead>
                                        <tr>
                                            <th>상품 번호</th>
                                            <th>투자액</th>
                                            <th>상환 계좌</th>
                                            <th>지불 계좌</th>
                                            <th>구매 날짜</th>
                                            <th>만기 날짜</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${ !empty myP2PList }">
                                        	<c:forEach items="${ myP2PList }" var="myP2P" varStatus="status">
		                                        <tr>
		                                            <td>${ myP2P.p2pNo }호</td>
		                                            <td>${ myP2P.investAmount }원</td>
		                                            <td>${ myP2P.repayAccount }</td>
		                                            <td>${ myP2P.payAccount }</td>
		                                            <td>${ myP2P.sold_date }</td>
		                                            <td>${ myP2P.due_date }</td>
		                                        </tr>
	                                        </c:forEach>
	                                    </c:if>
                                        </tbody>
                                    </table>
                                </div>
								<div class="table-responsive hidden-xs">
                                    <table id="investment_summary" class="table table_summary table_repayment">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>상환완료</th>
                                                <th>상환예정</th>
                                                <th>합계</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span>원금</span></td>
                                                <td>0 원</td>
                                                <td>0 원</td>
                                                <td>0 원</td>
                                            </tr>
                                            <tr>
                                                <td><span>이자 (세후, 수수료후)</span></td>
                                                <td>0 원</td>
                                                <td>0 원</td>
                                                <td>0 원</td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td><span>평균 수익률 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="투자한 채권의 평균 수익률을 연환산한 값입니다."></i></span></td>
                                                <td>연 0 %</td>
                                                <td>연 0 %</td>
                                                <td>연 0 %</td>
                                            </tr>
                                            <tr>
                                                <td><span>(-) 손실 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="발생했거나 발생할 것으로 예상되는 손실액이 수익률에 미치는 영향도 입니다."></i></span></td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                            </tr>
                                            <tr>
                                                <td><span>(+) 세이프플랜 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="세이프플랜으로 보호되는 금액이 수익률에 미치는 영향도 입니다."></i></span></td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                            </tr>
                                            <tr>
                                                <td><span>(-) 세금 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="발생했거나 발생할 것으로 예상되는 세금이 수익률에 미치는 영향도 입니다."></i></span></td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                            </tr>
                                            <tr>
                                                <td><span>(-) 투자 수수료 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="투자수수료가 수익률에 미치는 영향도 입니다."></i></span></td>
                                                <td>0 %p</td>
                                                <td>0 %p</td>
                                                <td>0%p</td>
                                            </tr>
                                        <tfoot>
                                            <tr>
                                                <th><span><strong>실질 수익률 <i class="icon-question" data-toggle="tooltip" data-placement="right" title="평균 수익률에서 예상 손실 정도와 세금, 투자수수료 등을 제한 수익률입니다."></i></strong></span></th>
                                                <td class="text-blue">연 0%</td>
                                                <td class="text-blue">연 0 %</td>
                                                <td class="text-blue">연 0 %</td>
                                            </tr>
                                        </tfoot>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>

</div>

<jsp:include page="../include/footer.jsp"/>