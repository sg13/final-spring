<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>가입 완료</h1>
    </div>
</section>

<div class="outLayer">
	<div class="result">
		<img src="${ pageContext.request.contextPath }/resources/img/check.ico" class="checkImg">
		<h2>${ custId }고객님, 지금까지 이용해주셔서 감사합니다.</h2>
		<hr>
	
	<input type="button" class="orderCompleteBtn" value="메인 홈페이지 바로가기" onclick="location.href='/index.jsp'">	
	<input type="button" class="orderCompleteBtn" value="상품 목록 살펴보기" onclick="location.href='/member/mypage'">
		
	</div>
</div>

<jsp:include page="../include/footer.jsp"/>