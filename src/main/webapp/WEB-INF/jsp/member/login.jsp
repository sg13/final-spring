<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>로그인</h1>
    </div>
</section>

<div class="outLayer">
	<section id="hero" class="login-header">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="login">
                   		<div class="text-center">
                               <h3 style="margin-top: 0px; margin-bottom: 20px;">로그인</h3>
                           </div>
						<form method="post">
							<div class="form-group input-group">
								<span class="input-group-addon"><img src="${ pageContext.request.contextPath }/resources/img/id.png" class="loginIcon"/></span>
								<input type="text" class=" form-control" id="user_id" name="id" placeholder="아이디를 입력해주세요">
							</div>
							<div class="form-group input-group">
								<span class="input-group-addon"><img src="${ pageContext.request.contextPath }/resources/img/pw.png" class="loginIcon"/></span>
                                   <input type="password" class=" form-control" id="user_password" name="password" placeholder="비밀번호를 입력해주세요">
							</div>
							<!-- <button type="button" class="btn_full" id="로그인">로그인</button> -->
							<input type="submit" class="btn_full" id="로그인" value="로그인"/>
							<a href="/member/join" class="btn_full_outline" id="회원가입">회원가입</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<jsp:include page="../include/footer.jsp"/>
