<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>투자의 달인 따라하기</title>

  <!-- Bootstrap -->
  <link href="${ pageContext.request.contextPath }/resources/css/order.css" rel="stylesheet">
  <link rel="stylesheet" href="${ pageContext.request.contextPath }/resources/css/animate.css">
  <link href="${ pageContext.request.contextPath }/resources/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="${ pageContext.request.contextPath }/resources/css/font-awesome.min.css">
  <link rel="stylesheet" href="${ pageContext.request.contextPath }/resources/css/font-awesome.css">
  <link rel="stylesheet" href="${ pageContext.request.contextPath }/resources/css/jquery.bxslider.css">
  <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/resources/css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/resources/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/resources/css/set1.css" />
  <link href="${ pageContext.request.contextPath }/resources/css/css-circular-prog-bar.css" rel="stylesheet">
  <link href="${ pageContext.request.contextPath }/resources/css/overwrite.css" rel="stylesheet">
  <link href="${ pageContext.request.contextPath }/resources/css/style.css" rel="stylesheet">
  <link href="${ pageContext.request.contextPath }/resources/css/mypage.css" rel="stylesheet">
  <link href="${ pageContext.request.contextPath }/resources/css/final.css" rel="stylesheet">
  <link rel="stylesheet" href="${ pageContext.request.contextPath }/resources/css/list.css"/>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"
   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
   crossorigin="anonymous"></script>
  <script src="${ pageContext.request.contextPath }/resources/js/final.js"></script>
  <script src="${ pageContext.request.contextPath }/resources/js/join.js"></script>
  <!-- =======================================================
    Theme Name: eNno
    Theme URL: https://bootstrapmade.com/enno-free-simple-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
<header>
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
        <a class="navbar-brand" href="${ pageContext.request.contextPath }/index.jsp"><img src="${ pageContext.request.contextPath }/resources/img/younghana.png"></a>
      </div>
      <div class="navbar-collapse collapse">
        <div class="menu">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="/p2p">상품목록</a></li>
            <li role="presentation"><a href="/notice">공지게시판</a></li>
            <li role="presentation"><a href="/board">문의게시판</a></li>
            <li role="presentation"><a href="/replyBoard">커뮤니티</a></li>
			<c:choose>
				<c:when test="${ empty userVO }">
		            <li role="presentation"><a href="/member/join">투달딸 시작하기</a></li>
		            <li role="presentation"><a href="/login">로그인</a></li>
				</c:when>
				<c:otherwise>
		            <li role="presentation"><a href="/mypage">마이페이지</a></li>
		            <li role="presentation"><a href="/logout">로그아웃</a></li>
					[${ userVO.id }님 환영합니다]
				</c:otherwise>
			</c:choose>
			
          </ul>
        </div>
      </div>
    </div>
  </nav>
</header>

