<jsp:include page="../include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section id="hero_2" class="invest-pased-wrap main-300">
    <div class="intro_title animated fadeInDown">
        <h1>문의 게시판 작성</h1>
    </div>
</section>

		<div class="container margin_60">
			<div class="row">
			<aside class="col-md-3 add_bottom_30">
					<div class="widget" id="cat_blog">
						<h4>Categories</h4>
						<hr>
						<ul>
							<li><a href="./news?mode=story">공지사항</a></li>
							<li><a href="./news?mode=media">언론보도</a></li>
						</ul>
					</div><!-- End widget -->
					
				</aside><!-- End aside -->
			
				<div class="col-md-8 col-sm-8">
					<div class="form_title">
						<h3><strong><i class="icon-pencil"></i></strong>문의글 작성</h3>
						<p></p>
					</div>
					<div class="step">
						<form:form method="post" commandName="boardVO" action="${ pageContext.request.contextPath }/board/write">
							<div class="row">
								<div class="col-sm-10">
									<div class="form-group">
										<label>제목</label>
										<!-- <input type="text" class="form-control" id="title" name="title" placeholder="제목을 입력해주세요"> -->
										<form:input path="title" class="form-control" id="title" name="title" placeholder="제목을 입력해주세요"/><form:errors path="title" class="red"/>
									</div>
								</div>
								<div class="col-sm-10">
									<div class="form-group">
										<label>작성자</label>
										<c:out value="${ userVO.id }" />
										<form:hidden path="writer" value="${ userVO.id }"/>
									</div>
								</div>
							</div>
							<!-- End row -->
							<div class="row">
								<div class="col-sm-10">
									<div class="form-group">
										<label>문의내용</label>
										<!-- <textarea rows="5" id="content" name="content" class="form-control" placeholder="문의내용을 입력해주세요" style="height:200px;"></textarea> -->
										<form:textarea path="content" id="content" name="content" class="form-control" placeholder="문의내용을 입력해주세요" style="height:200px;"/>
										<form:errors path="content" class="red"/>
									</div>
								</div>
							</div>
<!-- 							<button type="button" class="btn_1 green medium" id="문의하기">저장</button> -->
							<button type="submit" class="btn_1 green medium" id="문의하기">등록</button>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		
<jsp:include page="../include/footer.jsp"/>