<jsp:include page="/WEB-INF/jsp/include/header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<style>
.navbar-fixed-top {
    top: 0;
    border-width: 0 0 0;
    background-color: white;
    box-shadow: 0 2px 10px -2px rgba(0, 0, 0, 0.41);
    height: 80px;
    vertical-align: middle;
    padding: 30px 0;
    border-bottom: none !important;
    position: fixed;
    right: 0;
    left: 0;
    z-index:1030;
}

.nav-tabs > li > a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 0;
    border-radius: 0 0 0 0;
    text-transform: uppercase;
    /* font-weight: 600; */
    color: #666666;
    font-size: 20px;
    height: 80px;
    vertical-align: middle;
}
.nav > li > a {
    position: relative;
    display: block;
    padding: 0 25px;
    vertical-align: middle;
}
.progress-bar-info{
	background-color: #008485;
}
.progress-bar{
	float: left;
	height: 100%;
	font-size: 12px;
	line-height: 20px;
	color: #fff;
	text-align: center;
}

</style>
 
    <div class="row">
      <div class="slider">
        <div class="img-responsive">
          <ul class="bxslider">
            <li>
            	<div class="slide">
            		<div class="noticeSlide"><img src="${ pageContext.request.contextPath }/resources/img/main.png"></div>
            	</div>
            </li>
            <li>
            	<div class="slide">
            		<div class="noticeContent">
            			<div class="noticeSlide">P2P(peer-to-peer)란 무엇인가?</div>
            		</div>
            		<span class="noticeImg">
            			<img src="${ pageContext.request.contextPath }/resources/img/illu.png">
            		</span>
            	</div>
            </li>
            <li>
            	<div class="slide">
            		<div class="noticeSlide">P2P(peer-to-peer)란 무엇인가?</div>
            	</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  
<div class="outLayer">
<div class="mainBox">
<div class="item-info">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="text-center">
          <h2>장인가구 00점 (6개월)</h2>
        </div>
      </div>
    </div>
  </div>
  <hr>

  <div class="container">
    <div class="row">
      <div class="box">
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.4s">
            <h4>상환기간 6개월</h4>
            <div class="icon">
              <img src="${ pageContext.request.contextPath }/resources/img/cal.png"></img>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.0s">
            <h4>연 수익률 9.55%</h4>
            <div class="icon">
              <img src="${ pageContext.request.contextPath }/resources/img/money.png"></img>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.6s">
            <h4>하나 등급</h4>
            <div class="icon">
              <h1>B+</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<hr>

<div class="progress">
	<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
		aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
		50% Complete (info)
	</div>
</div>
  
  <div class="container">
    <div class="row">
      <div class="box">
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.4s">
            <h4>상품 종류</h4>
            <div class="icon">
              <h1>개인 사업자</h1>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.0s">
            <h4>월 상환금</h4>
            <div class="icon">
              <h1>168만원</h1>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.6s">
            <h4>상환방식</h4>
            <div class="icon">
              <h1>원리금 균등</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<hr>

<div class="container">
	<div class="row">
		<div class="col-md-3">
	    	<h3 style="font-weight:400; color:#333">대출 목적</h3>
	    </div>
	    <div class="col-md-9">
	        <p class="txt-box">
	        	대출목적
	        </p>
		</div>
	</div>
	<hr>



	  <div id="상점소개">
	    <!--상점소개시작-->
	    <div class="row">
	        <div class="col-md-3">
	            <h3>상점소개</h3>
	        </div>
	        <div class="col-md-9">
	            <div class="row" style="margin: 0px;">
	                <div class="col-md-6 col-sm-6">
	                    <ul class="list_ok introduce">
	                        <li>운영기간: 0년 11개월</li>
	                        <li>직원수: 4명</li>
	                        <li>운영시간: 10:00~23:00</li>
	                    </ul>
	                </div>
	                <div class="col-md-6 col-sm-6">
	                    <ul class="list_ok introduce">
	                        <li>매장규모: 20평</li>
	                        <li id="store_address">주소:  서울 송파구 가락로 170</li>
	                    </ul>
	                </div>
	            </div>
	            <!-- End row  -->
	        </div>
	    </div>
	    <hr>
	    <!--상점소개끝-->
		</div>

	<div class="row">
		<div class="col-md-3">
	    	<h3 style="font-weight:400; color:#333">투자 위험 고지</h3>
	    </div>
	    <div class="col-md-9">
	        <p class="txt-box">
	        	펀다는 투자 원금과 수익을 보장하지 않으며, 투자 손실에 대한 책임은 투자자에게 있습니다.
	                        펀다의 상품 투자는 투자자가 펀다 플랫폼을 통해 대출 채권의 원금과 이자를 수취할 수 있는 권리(원리금 수취권)를 매입하는 것으로, 대출 채권의 상태 변경에 따라 수익률이 직접적인 영향을 받을 수 있습니다.
	        </p>
		</div>
	</div>
</div>
</div><!-- item-info -->
    
<div class="box_style_1" id="expose">
	<h3 class="inner">투자금액 설정</h3>
	<div class="input-amount">
		<div class="input-group">
			<input class="text-right form-control input" type="text" id="amount"
                   class='amount_invest' name="amount" value=""
                   placeholder="1만원 단위로 입력하세요" numberOnly/>
            <span class="input-group-addon">만원</span>
        </div>
    </div>
    <div class="add-amount">
        <input class="col-xs-3 btn" type="button" id="100" value="100만"  checked>
        <input class="col-xs-3 btn" type="button" id="10" value="10만"  checked>
        <input class="col-xs-3 btn" type="button" id="1" value="1만"  checked>
        <input class="col-xs-3 btn" type="button" id="0" value="정정"  checked>
    </div>

    <div class="money-range">
        <p>예상 수익금(세전)<span>90 만원</span></p>
    </div>
    <div class="my-money">
         <p>예상 수익금(실수령액) <span style="margin-bottom: 0px;"> 254원</span></p>
    </div>
    
	<button class="btn_full" type="button" id="투자하기">투자하기</button>
	<div class="text-center">
		<p style="text-decoration: underline;" type="button" id="상환일정보기" class="cursor-pointer text-blue">상환일정 보기<i class="icon-angle-right"></i></p>
    </div>
</div>
</div> <!-- mainBox -->
</div> <!-- outLayer -->

<jsp:include page="./WEB-INF/jsp/include/footer.jsp"/>