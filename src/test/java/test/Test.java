package test;

import java.util.List;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import kr.co.mlec.account.vo.AccountVO;
import kr.co.mlec.notice.vo.NoticeVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:config/**/*.xml"})
public class Test {

	@Autowired
	private SqlSessionTemplate session;
	
	@Ignore
	public void test() throws Exception {
		AccountVO accountVO = new AccountVO();
		accountVO.setAccount(885656);
		accountVO.setName("name");
		accountVO.setBank("국민");
		accountVO.setInvestAmount(10000);
		session.update("account.dao.AccountDAO.updateBalance", accountVO);
	}
	
	@Ignore
	public void test1() throws Exception {
		session.update("account.dao.AccountDAO.investCntChart");
	}
	
	@Ignore
	public void test2() throws Exception {
		AccountVO accountVO = new AccountVO();
		accountVO.setAccount(23424);
		accountVO.setName("name");
		accountVO.setBank("하나");
		accountVO.setDepositor("강아지");
		System.out.println(accountVO);
		
		session.update("account.dao.AccountDAO.checkAccount", accountVO);
	}
	
	@org.junit.Test
	public void test3() throws Exception {
		session.selectList("notice.dao.NoticeDAO.selectAll");
	}
	
	
	
}
